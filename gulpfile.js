/*
*	Task Automation to make my life easier.
*	Author: Jean-Pierre Sierens
*	Edit: Matthieu Pierre-Louis
*	===========================================================================
*/
// declarations, dependencies
// ----------------------------------------------------------------------------
const gulp = require('gulp');
const sass = require('gulp-sass');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const gutil = require('gulp-util');
const babelify = require('babelify');
const concat = require('gulp-concat');
const minify = require('gulp-minify');
const minifyCss = require('gulp-clean-css');

// Gulp tasks
// ----------------------------------------------------------------------------
gulp.task('js', () => {
  browserify({ entries: 'public/src/js/main.js', debug: true })
  .transform('babelify', { presets: ['airbnb'] })
  .bundle()
  .on('error', gutil.log)
  .pipe(source('main.js'))
  .pipe(gulp.dest('public/build/js'));
});

gulp.task('css', () => gulp.src('public/src/css/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('public/build/css')));

gulp.task('js:minify', () => gulp.src(['public/build/js/main.js'])
  .pipe(concat('main.min.js'))
  .pipe(minify())
  .pipe(gulp.dest('public/build/js')));

gulp.task('css:minify', () => gulp.src('public/src/css/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCss())
    .pipe(concat('main.min.css'))
    .pipe(gulp.dest('public/build/css')));

gulp.task('watch', () => {
  gulp.watch(['public/src/css/**/*.scss'], ['css']);
  gulp.watch(['public/src/js/*.js'], ['js']);
});

gulp.task('build', ['js:minify', 'css:minify']);
