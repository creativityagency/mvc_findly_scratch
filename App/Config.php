<?php

namespace App;

/**
 * Application configuration
 *
 * PHP version 5.4
 */
class Config
{

    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'findly';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'root';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = 'root';

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = true;

    /**
     * Secret key for hashing
     * @var boolean
     */
    const SECRET_KEY = 'GhfBwA2WKuGLflS25VvrdMkDsGQeCqSy'; //https://randomkeygen.com/

    /**
     * Mailgun API key
     *
     * @var string
     */
    const MAILGUN_API_KEY = 'key-de3ad13c74b3402b2c2a250a2c42ff54';

    /**
     * Mailgun domain
     *
     * @var string
     */
    const MAILGUN_DOMAIN = 'sandboxc307948028564e4889065f97e72a6d90.mailgun.org';
}
