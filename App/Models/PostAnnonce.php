<?php

namespace App\Models;

use PDO;

/**
 * Post model
 *
 * PHP version 5.4
 */
class PostAnnonce extends \Core\Model
{

    /**
     * Get all the posts as an associative array
     *
     * @return array
     */
    public static function getCategorie()
    {
        try {
            $db = static::getDB();

            $sql = 'SELECT * FROM categories';
            $db = static::getDB();
            $stmt = $db->prepare($sql);
            $stmt->execute();

            return $stmt->fetchAll();

        
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    /*
     * Insere les articles en BDD
     */
    public static function insertAnnonce($titre, $contenus, $photo_annonce, $id_Categories, $type_annonce, $date_publication, $nom, $prenom, $email)
    {
        try{

            $sql ="INSERT INTO annonces (titre, contenus, type_annonce, date_publication, photo_annonce, id_Categories, nom, prenom, email) 
                    VALUES (:titre, :contenus, :type_annonce, :date_publication, :photo_annonce, :id_Categories, :nom, :prenom, :email)";

            $db = static::getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':titre', $titre);
            $stmt->bindValue(':contenus', $contenus);
            $stmt->bindValue(':type_annonce', $type_annonce);
            $stmt->bindValue(':date_publication', $date_publication);
            $stmt->bindValue(':photo_annonce', $photo_annonce);
            $stmt->bindValue(':id_Categories', $id_Categories);
            $stmt->bindValue(':nom', $nom);
            $stmt->bindValue(':prenom', $prenom);
            $stmt->bindValue(':email', $email);

            $stmt->execute();

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    public static function updateGrade($grade, $id)
    {
        $sql = "UPDATE utilisateurs
                SET grade = grade + :grade
                WHERE id = :id";

        $db = static::getDB();
        $stmt = $db->prepare($sql);

        $stmt->bindValue(':grade', $grade);
        $stmt->bindValue(':id', $id);

        return $stmt->execute();
    }



















}
