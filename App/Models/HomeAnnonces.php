<?php

namespace App\Models;

use PDO;

/**
 * Post model
 *
 * PHP version 5.4
 */
class HomeAnnonces extends \Core\Model
{
    /**
     * Get all the posts as an associative array
     *
     * @return array
     */
    public static function getMaxId()
    {

        try {
            //$db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $username, $password);
            $db = static::getDB();

            //$stmt = $db->query('SELECT id, title, content FROM posts ORDER BY created_at');
            $stmt = $db->query('SELECT MAX(id) FROM annonces');
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getAnnonces($nombre)
    {

        try {
            //$db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $username, $password);
            $db = static::getDB();
            $a = 2;
            //$stmt = $db->query('SELECT id, title, content FROM posts ORDER BY created_at');
            $stmt = $db->query("SELECT * FROM annonces ORDER BY id DESC LIMIT ".$nombre." " );
            $results = $stmt->fetchAll();
            return $results;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    



}
