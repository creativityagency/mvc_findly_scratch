<?php

namespace App\Models;

use PDO;

/**
 * Post model
 *
 * PHP version 5.4
 */
class Infos extends \Core\Model
{

   /*
    * Recupere les Data particulier 
    */
    public static function getDataParticulier($email, $id)
    {
        $sql = 'SELECT * FROM utilisateurs WHERE mail = :email and id = :id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);
        $stmt->bindValue(':id', $id, PDO::PARAM_STR);

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function updateDataParticulierWithPassword($nom, $prenom, $password, $date_de_naissance, $id)
    {

                $sql = "UPDATE utilisateurs
        		SET nom = :nom,
        			prenom = :prenom,
        			date_naissance = :date_naissance,
        			password = :password
        		WHERE id = :id";

        $db = static::getDB();
        $stmt = $db->prepare($sql);

        $stmt->bindValue(':nom', $nom, PDO::PARAM_STR);
        $stmt->bindValue(':prenom', $prenom, PDO::PARAM_STR);
        $stmt->bindValue(':password', $password, PDO::PARAM_STR);
        $stmt->bindValue(':date_naissance', $date_de_naissance, PDO::PARAM_STR);
        $stmt->bindValue(':id', $id, PDO::PARAM_STR);

        return $stmt->execute();
    }

     public static function updateDataParticulierWithoutPassword($nom, $prenom, $date_de_naissance, $id)
    {
        $sql = "UPDATE utilisateurs
        		SET nom = :nom,
        			prenom = :prenom,
        			date_naissance = :date_naissance
        		WHERE id = :id";

        $db = static::getDB();
        $stmt = $db->prepare($sql);

        $stmt->bindValue(':nom', $nom, PDO::PARAM_STR);
        $stmt->bindValue(':prenom', $prenom, PDO::PARAM_STR);
        $stmt->bindValue(':date_naissance', $date_de_naissance, PDO::PARAM_STR);
        $stmt->bindValue(':id', $id, PDO::PARAM_STR);

        return $stmt->execute();
    }






}
