<?php

namespace App\Models;

use PDO;

/**
 * Post model
 *
 * PHP version 5.4
 */
class Ad extends \Core\Model
{

   /*
    * Recupere les Categories
    */
    public static function getCategories()
    {
        $sql = 'SELECT * FROM categories';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public static function getAnnonces($id, $order)
    {
        $sql = 'SELECT * FROM annonces WHERE id_Categories = :id ORDER BY :order DESC';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':id', $id, PDO::PARAM_STR);
        $stmt->bindValue(':order', $order, PDO::PARAM_STR);

        $stmt->execute();

        return $stmt->fetchAll();
    }

    public static function updateFindUp($id)
    {
        $sql = 'UPDATE annonces
				SET find_up = find_up + 1
				WHERE id = :id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':id', $id, PDO::PARAM_STR);
        $stmt->execute();
    }









    public static function getDataParticulier($email, $id)
    {
        $sql = 'SELECT * FROM utilisateurs WHERE mail = :email and id = :id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);
        $stmt->bindValue(':id', $id, PDO::PARAM_STR);

        $stmt->execute();

        return $stmt->fetch();
    }
















    public static function updateDataParticulierWithPassword($nom, $prenom, $password, $date_de_naissance, $id)
    {

                $sql = "UPDATE utilisateurs
        		SET nom = :nom,
        			prenom = :prenom,
        			date_naissance = :date_naissance,
        			password = :password
        		WHERE id = :id";

        $db = static::getDB();
        $stmt = $db->prepare($sql);

        $stmt->bindValue(':nom', $nom, PDO::PARAM_STR);
        $stmt->bindValue(':prenom', $prenom, PDO::PARAM_STR);
        $stmt->bindValue(':password', $password, PDO::PARAM_STR);
        $stmt->bindValue(':date_naissance', $date_de_naissance, PDO::PARAM_STR);
        $stmt->bindValue(':id', $id, PDO::PARAM_STR);

        return $stmt->execute();
    }

     public static function updateDataParticulierWithoutPassword($nom, $prenom, $date_de_naissance, $id)
    {
        $sql = "UPDATE utilisateurs
        		SET nom = :nom,
        			prenom = :prenom,
        			date_naissance = :date_naissance
        		WHERE id = :id";

        $db = static::getDB();
        $stmt = $db->prepare($sql);

        $stmt->bindValue(':nom', $nom, PDO::PARAM_STR);
        $stmt->bindValue(':prenom', $prenom, PDO::PARAM_STR);
        $stmt->bindValue(':date_naissance', $date_de_naissance, PDO::PARAM_STR);
        $stmt->bindValue(':id', $id, PDO::PARAM_STR);

        return $stmt->execute();
    }






}
