<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>{% block title %}{% endblock %}</title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/domready/1.0.8/ready.min.js"></script>
  <script src="/findly/public/build/js/main.js"></script>

  <link rel="stylesheet" href="/findly/public/build/css/style.css"></link>

  <!-- A PURGER -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

</head>

<body>


  <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">

      <ul class="navbar-nav">
        <li class="nav-item active">
            <a class="nav-link" href="/findly/connexion/particulier/grade">Home <span class="sr-only">(current)</span></a>
        </li>

        <li class="nav-item active">
            <a class="nav-link" href="/findly/connexion/particulier/post/annonce">Poster une annonce <span class="sr-only">(current)</span></a>
        </li>

        <li class="nav-item active">
            <a class="nav-link" href="/findly/connexion/particulier/show/annonce/categories">Voir les annonces <span class="sr-only">(current)</span></a>
        </li>

        <li class="nav-item active">
            <a class="nav-link" href="/findly/connexion/particulier/compte/informations">Modifier mes informations <span class="sr-only">(current)</span></a>
        </li>
    
        <li class="nav-item active">
            <a class="nav-link" href="/findly/connexion/particulier/deconnexion">Se deconnecter <span class="sr-only">(current)</span></a>
        </li>



      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-md-auto">
          {% if current_user %}
          <li class="nav-item">
            <a class="nav-link" href="/findly/profile/show">Profile</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/findly/logout">Log out</a>
          </li>
          {% endif %}
        </ul>
      </div>
    </nav>


  <div class="container-fluid">
    {% for message in flash_messages %}
    <div class="alert alert-{{ message.type }}">
      {{ message.body }}
    </div>
    {% endfor %}





    {% block body %}
    			  <h1>Modifier mes informations</h1>

				    <!-- la methode create du controlleur connexion -->
				  <form method="post" action="/findly/connexion/particulier/compte/informations/validate">

				    <div class="form-group">
				      <label for="input">Nom</label>
				      <input type="text" id="inputEmail" value="{{data.nom}}" name="nom" class="form-control" placeholder="Nom">
				    </div>

				    <div class="form-group">
				      <label for="input">Prénom</label>
				      <input type="text" id="inputEmail" value="{{data.prenom}}" name="prenom" class="form-control" placeholder="Prénom">
				    </div>

				    <div class="form-group">
				      <label for="input">Mot de passe ( /!\ Ne saisissez votre mot de passe seulement si vous souhaitez le modifier car celui n'apparaît pas pour votre sécurité)</label>
				      <input type="password" id="inputEmail" name="mdp" class="form-control" placeholder="Mot de passe">
				    </div>

				    <div class="form-group">
				      <label for="inputPassword">Date de naissance : Format (YYYY-MM-DD)</label>
				      <input type="date" id="inputPassword" value="{{data.date_naissance}}" name="date_naissance" class="form-control" placeholder="Date de naissance">
				    </div>

				  

				    <button class="btn btn-default" type="submit" name="update_data">Modifier mes informations</button>

				  </form>



    {% endblock %}
    {% block footer %}
    {% endblock %}
  </div>
</body>

</html>
