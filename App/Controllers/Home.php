<?php

namespace App\Controllers;

use \Core\View;
use \App\Auth;
use \App\Models\HomeAnnonces;

/**
 * Home controller
 *
 * PHP version 5.4
 */
class Home extends \Core\Controller
{
    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        $nombre = 10;
        $tab_annonces = HomeAnnonces::getAnnonces($nombre);

        
        View::renderTemplate('Home/index.html', [
                'data' => $tab_annonces
            ]);
    }
}
