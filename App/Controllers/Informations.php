<?php

namespace App\Controllers;

use \Core\View;
use \App\Auth;
use \App\Flash;
use \App\Models\Infos;

/**
 * Home controller
 *
 * PHP version 5.4
 */
class Informations extends \Core\Controller
{
    /*
     * Affiche le formulaire avec les informations d'origine
     *
     */
    public function particulierFormAction()
    {
        if ($_SESSION["connection"] && $_SESSION["role"] == "ROLE_PARTICULIER")
        {
            $data = Infos::getDataParticulier($_SESSION["email"], $_SESSION["id"]);
            View::renderTemplate('Infos/particulier.php', [
                 'data' => $data
            ]);              
        } 
        else{
            View::renderTemplate('Login/particulier.html');
            echo "Erreur identification / Serveur";
        }
    }

    /*
     * Recupere les DATA des particuliers apres la confirmation du formulaire
     */
    public function particulierRecupFormUpdateAction()
    {
        if ($_SESSION["connection"] && $_SESSION["role"] == "ROLE_PARTICULIER")
        {
            if (isset($_POST["update_data"]))
            {
                if (!empty($_POST["mdp"])){
                    $password = hash('sha512', $_POST['mdp']);
                    $nom = $_POST["nom"];
                    $prenom = $_POST["prenom"];
                    $date_de_naissance = $_POST["date_naissance"];

                    Infos::updateDataParticulierWithPassword($nom, $prenom, $password, $date_de_naissance, $_SESSION["id"]);

                    Flash::addMessage('Modifications effectuées avec succès');
                    $this->redirect('/findly/connexion/particulier/compte/informations');
                } else {
                    $nom = $_POST["nom"];
                    $prenom = $_POST["prenom"];
                    $date_de_naissance = $_POST["date_naissance"];

                    Infos::updateDataParticulierWithoutPassword($nom, $prenom, $date_de_naissance, $_SESSION["id"]);

                    Flash::addMessage('Modifications effectuées avec succès');
                    $this->redirect('/findly/connexion/particulier/compte/informations');
                }
            }
        } 
        else{
            View::renderTemplate('Login/particulier.html');
            echo "Erreur identification / Serveur";
        }
    }





}
