<?php

namespace App\Controllers;

use \Core\View;
use \App\Models\User;
use \App\Auth;
use \App\Flash;

/**
 * Account controller
 *
 * PHP version 7.0
 */
class Connexion extends \Core\Controller
{

/* DEBUT: AFFICHAGE DES FORMULAIRES */

    /**
     * Show the login page for particulier
     *
     * @return void
     */
    public function particulierAction()
    {
        View::renderTemplate('Login/particulier.html');
    }

     /**
     * Show the login page for professionnel
     *
     * @return void
     */
    public function professionnelAction()
    {
        View::renderTemplate('Login/professionnel.html');
    }

/* FIN: AFFICHAGE DES FORMULAIRES */



    /**
     * TEST LA VALIDITE DU FORMULAIRE ET REDIRIGE
     * Log in : particulier et admin
     *
     * @return void
     */
    public function routerRoleConnectAction()
    {
        if (isset($_POST["particulier"]))
        {
            $_SESSION["email"] = $_POST['email'];
            $hash_mdp = hash('sha512', $_POST['password']);
            $_SESSION["password"] = $hash_mdp;

            // Test si les infos sont correts ou non
            $_SESSION["connection"] = User::authenticateParticulier($_SESSION["email"], $_SESSION["password"]);
            if ($_SESSION["connection"]){
                $data = User::getDataParticulier($_SESSION["email"], $_SESSION["password"]);
                $_SESSION["data"] = $data;
                $_SESSION["role"] = $data["role"];
                $_SESSION["nom"] = $data["nom"];
                $_SESSION["prenom"] = $data["prenom"];
                $_SESSION["id"] = $data["id"];
                $_SESSION["grade"] = $data["grade"];


                // Retourne un message 
                    //Flash::addMessage('Login successful');
                // Gestion du type de ROLE (ADMIN ou PARTICULIER)
                if ($_SESSION["role"] == "ROLE_PARTICULIER"){
                        View::renderTemplate('Login/particulierBO.html', [
                        'data' => $data
                    ]);
                }

                elseif ($_SESSION["role"] == "ROLE_ADMIN"){
                    View::renderTemplate('Login/administrateurBO.html', [
                        'email' => $data["mail"],
                        'nom' => $data["nom"],
                        'prenom' => $data["prenom"],
                        'role' => $data["role"]
                    ]);
                }

                else{
                    Flash::addMessage("Espace membre reservée aux PARTICULIERS, veuillez changer d'espace de connexion");
                    $this->redirect('/findly/');
                }

            } 
            else{
                View::renderTemplate('Login/particulier.html');
                echo "Erreur identifiants";
            }
        }

        // Gestion du type de ROLE (PRO) a terminer
        if (isset($_POST["pro"]))
        {
            var_dump("OK");
            if ($_SESSION["role"] == "ROLE_PRO"){
                    View::renderTemplate('Login/professionnelBO.html');
            }
        }


    }

    public function particulierGradeAction()
    {
        if ($_SESSION["connection"] && $_SESSION["role"] == "ROLE_PARTICULIER")
        {
            View::renderTemplate('Login/particulierBO.html', [
                        'data' => $_SESSION["data"]
                    ]);
        } 
        else{
            View::renderTemplate('Login/particulier.html');
            echo "Erreur identification / Serveur";
        }
    }

    /**
     * Deconnecte l'utilisateur particulier
     *
     * @return void
     */
    public function deconnexionAction()
    {
        session_destroy();
        Flash::addMessage('Logout successful');
        $this->redirect('/findly/');
        
    }















    /**
     * Show a "logged out" flash message and redirect to the homepage. Necessary to use the flash messages as they use the session and at the end of the logout method (destroyAction) the session is destroyed so a new action needs to be called in order to use the session.
     *
     * @return void
     */
    public function showLogoutMessageAction()
    {
        Flash::addMessage('Logout successful');

        $this->redirect('/findly/');
    }
}
