<?php

namespace App\Controllers;

use \Core\View;
use \App\Auth;
use \App\Flash;
use \App\Models\Ad;

/**
 * Home controller
 *
 * PHP version 5.4
 */
class Ads extends \Core\Controller
{
    public function showCategoriesAction()
    {
        if ($_SESSION["connection"] && $_SESSION["role"] == "ROLE_PARTICULIER")
        {
        	$data = Ad::getCategories();

            View::renderTemplate('Ad/categories.html', [
            	'data' => $data
            ]);              
        } 
        else{
            View::renderTemplate('Login/particulier.html');
            echo "Erreur identification / Serveur";
        }
    }

    public function showAnnoncesAction()
    {
        if ($_SESSION["connection"] && $_SESSION["role"] == "ROLE_PARTICULIER")
        {
        	if (isset($_POST['annonce']))
        	{
        		$_SESSION['categ'] = $_POST['categorie'];
        		
        	}

        	$data = Ad::getAnnonces($_SESSION['categ'], "date");
            View::renderTemplate('Ad/annonces.html' , [
            	'data' => $data
            ]);              
        } 
        else{
            View::renderTemplate('Login/particulier.html');
            echo "Erreur identification / Serveur";
        }
    }

    /*Permet de filtrez - attention parametre provient de la sesison avec l id dedans*/
    public function showWithFiltreAction()
    {
        if ($_SESSION["connection"] && $_SESSION["role"] == "ROLE_PARTICULIER")
        {
            if (isset($_POST['date']))
            {
                $filtre = "date_publication";
  
                
                $data = Ad::getAnnonces($_SESSION['categ'], $filtre );
        
                View::renderTemplate('Ad/annonces.html' , [
                    'data' => $data
                ]); 
            }
            if (isset($_POST['findup']))
            {
                $filtre = "find_up";
         
                $data = Ad::getAnnonces($_SESSION['categ'], $filtre );
             
                View::renderTemplate('Ad/annonces.html' , [
                    'data' => $data
                ]); 
            }

                         
        } 
        else{
            View::renderTemplate('Login/particulier.html');
            echo "Erreur identification / Serveur";
        }
    }

function updateFindUp()
{
    if ($_SESSION["connection"] && $_SESSION["role"] == "ROLE_PARTICULIER")
        {
            if (isset($_POST['findup_update']))
            {
               var_dump($_POST["findup_update"]);
               var_dump($_POST["findup_calcul"]);
               $id = $_POST["findup_calcul"];
               Ad::updateFindUp($id);
               $this->redirect('/findly/connexion/particulier/show/annonce/categories/annonces');
            }              
        } 
        else{
            View::renderTemplate('Login/particulier.html');
            echo "Erreur identification / Serveur";
        }
}




/*
    public function particulierAddAction()
    {
        if ($_SESSION["connection"] && $_SESSION["role"] == "ROLE_PARTICULIER")
        {
        	if (isset($_POST["annonce"]))
        	{
        		if (!empty($_POST["titre"]) && !empty($_FILES["image"]) && !empty($_POST["message"]) && !empty($_POST["categorie"]) )
        		{
        			$dossier = 'C:\wamp64\www\findly\public\src\upload\up-';
        	
        			// Recupere l'extension du fixhier telechargé
        			$extension = strrchr($_FILES['image']['name'], '.');
        			$extensions = array('.png', '.gif', '.jpg', '.jpeg');
        			

        			if (in_array($extension, $extensions))
        			{
        				// Copie l'image temporaire sur le serveur FINDLY
        				copy($_FILES['image']['tmp_name'], $dossier . $_SESSION["nom"] . $_SESSION["email"] . date("d-m-Y") . date("H-i-s") . $extension );
        				$lien_image = $dossier . $_SESSION["nom"] . $_SESSION["email"] . date("d-m-Y") . date("H-i-s") . $extension;



        				$_SESSION["titre"] = $_POST["titre"];
	        			$_SESSION["message"] = $_POST["message"];
	        			$_SESSION["image"] = $_FILES["image"];
	        			$_SESSION["categorie"] = $_POST["categorie"];


	        			// Insere en BDD l'annonce 
        				PostAnnonce::insertAnnonce($_SESSION["titre"], $_SESSION["message"], $lien_image, $_SESSION["categorie"], $_SESSION["role"], date("Y-m-d"), $_SESSION["nom"], $_SESSION["prenom"], $_SESSION["email"] );

        				// Met a jour le grade en bdd
        				$grade = 1;
        				PostAnnonce::updateGrade($grade, $_SESSION["id"]);

        				// Redirige apres l'insertions
        				Flash::addMessage("Merci de votre publication");
                    	$this->redirect('/findly/connexion/particulier/grade');

        			}
        			else
        			{
        				echo "ERREUR SUR L'EXTENSION DU FICHIER";
        			}
        			var_dump($extension);

        			
        		}
        		else
        		{
        			Flash::addMessage("Tout les champs doivent être plein");
        			$this->redirect('/findly/connexion/particulier/post/annonce');
        		}
        		
        	}
        } 
        else{
            View::renderTemplate('Login/particulier.html');
            echo "Erreur identification / Serveur";
        }
    }
*/


}
