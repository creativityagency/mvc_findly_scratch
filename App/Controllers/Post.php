<?php

namespace App\Controllers;

use \Core\View;
use \App\Auth;
use \App\Flash;
use \App\Models\PostAnnonce;

/**
 * Home controller
 *
 * PHP version 5.4
 */
class Post extends \Core\Controller
{
    public function particulierAction()
    {
        if ($_SESSION["connection"] && $_SESSION["role"] == "ROLE_PARTICULIER")
        {
        	$data = PostAnnonce::getCategorie();
            View::renderTemplate('Post/index.html', [
            	'data' => $data
            ]);              
        } 
        else{
            View::renderTemplate('Login/particulier.html');
            echo "Erreur identification / Serveur";
        }
    }

    public function particulierAddAction()
    {
        if ($_SESSION["connection"] && $_SESSION["role"] == "ROLE_PARTICULIER")
        {
        	if (isset($_POST["annonce"]))
        	{
        		if (!empty($_POST["titre"]) && !empty($_FILES["image"]) && !empty($_POST["message"]) && !empty($_POST["categorie"]) )
        		{
        			$dossier = 'C:\wamp64\www\findly\public\src\upload\up-';
        	
        			// Recupere l'extension du fixhier telechargé
        			$extension = strrchr($_FILES['image']['name'], '.');
        			$extensions = array('.png', '.gif', '.jpg', '.jpeg');
        			

        			if (in_array($extension, $extensions))
        			{
        				// Copie l'image temporaire sur le serveur FINDLY
        				$a = copy($_FILES['image']['tmp_name'], $dossier . $_SESSION["nom"] . $_SESSION["email"] . date("d-m-Y") . date("H-i-s") . $extension );

                        // Renomme l'image en BDD
        				$lien_image = "/findly/public/src/upload/up-" . $_SESSION["nom"] . $_SESSION["email"] . date("d-m-Y") . date("H-i-s") . $extension;

                        var_dump($a);
                        var_dump($lien_image);

        				$_SESSION["titre"] = $_POST["titre"];
	        			$_SESSION["message"] = $_POST["message"];
	        			$_SESSION["image"] = $_FILES["image"];
	        			$_SESSION["categorie"] = $_POST["categorie"];


	        			// Insere en BDD l'annonce 
        				PostAnnonce::insertAnnonce($_SESSION["titre"], $_SESSION["message"], $lien_image, $_SESSION["categorie"], $_SESSION["role"], date("Y-m-d"), $_SESSION["nom"], $_SESSION["prenom"], $_SESSION["email"] );

        				// Met a jour le grade en bdd
        				$grade = 1;
        				PostAnnonce::updateGrade($grade, $_SESSION["id"]);

        				// Redirige apres l'insertions
        				Flash::addMessage("Merci de votre publication");
                    	$this->redirect('/findly/connexion/particulier/grade');

        			}
        			else
        			{
        				echo "ERREUR SUR L'EXTENSION DU FICHIER";
        			}
        			
        		}
        		else
        		{
        			Flash::addMessage("Tout les champs doivent être plein");
        			$this->redirect('/findly/connexion/particulier/post/annonce');
        		}
        		
        	}
        } 
        else{
            View::renderTemplate('Login/particulier.html');
            echo "Erreur identification / Serveur";
        }
    }



}
