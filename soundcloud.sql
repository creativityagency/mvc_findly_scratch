-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Mar 24 Janvier 2017 à 22:57
-- Version du serveur :  5.5.49-log
-- Version de PHP :  7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `soundcloud`
--

-- --------------------------------------------------------

--
-- Structure de la table `songs`
--

CREATE TABLE IF NOT EXISTS `songs` (
  `id` int(10) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `artwork` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `songs`
--

INSERT INTO `songs` (`id`, `url`, `username`, `title`, `artwork`) VALUES
(1, 'https://api.soundcloud.com/tracks/272802309/stream?client_id=892031c10866d66948c9a34b24b9fdf7', 'admin', 'Tory Lanez - Controlla', 'https://i1.sndcdn.com/artworks-000172182566-cjd8iy-large.jpg'),
(2, 'https://api.soundcloud.com/tracks/300541034/stream?client_id=892031c10866d66948c9a34b24b9fdf7', 'admin', 'Tory Lanez - Bal Harbour (Feat. A Ferg) (Prod. C Sick)', 'https://i1.sndcdn.com/artworks-000200878726-6tm08q-large.jpg'),
(3, 'https://api.soundcloud.com/tracks/194491283/stream?client_id=892031c10866d66948c9a34b24b9fdf7', 'toto', 'Two-9 – Full House ft. Wiz Khalifa & Ty Dolla Sign', 'https://i1.sndcdn.com/artworks-000105625354-07wyjk-large.jpg'),
(4, 'https://api.soundcloud.com/tracks/278264307/stream?client_id=892031c10866d66948c9a34b24b9fdf7', 'toto', 'Tory Lanez - Tim Duncan (Prod. C-Sick)', 'https://i1.sndcdn.com/artworks-000149689140-0a43et-large.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `roles`) VALUES
(1, 'admin', '$2y$13$GbW1/LKOC7BJx4WX4GhAY.PBeQyImzyhRcHUbKmLhOEq641KQ.Rp2', 'ROLE_ADMIN'),
(2, 'toto', '$2y$13$GbW1/LKOC7BJx4WX4GhAY.PBeQyImzyhRcHUbKmLhOEq641KQ.Rp2', 'ROLE_USER');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `songs`
--
ALTER TABLE `songs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_BAECB19BF47645AE` (`url`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1483A5E9F85E0677` (`username`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `songs`
--
ALTER TABLE `songs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
