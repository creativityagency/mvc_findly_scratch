var prev, next, slides, position;

function Previous() {
  if (position > 1) {
    position -= 1;
    for (var i = 0; i < slides.length; i++) {
      slides[i].style.transform = 'translateX(-' + ((position - 1) * 21) + 'vw)';
    }
  } else if (position == 1) {
    for (var j = 0; j < slides.length; j++) {
      slides[j].style.transform = 'translateX(0vw)';
    }
  }
}

function Next() {
  if (position < (slides.length - 2)) {
    position += 1;
    for (var i = 0; i < slides.length; i++) {
      slides[i].style.transform = 'translateX(-' + ((position - 1) * 21) + 'vw)';
    }
  }
}

window.addEventListener('DOMContentLoaded', function () {
  prev = document.querySelector('#prev');
  next = document.querySelector('#next');
  slides = document.querySelectorAll('#slider > .slide');
  position = 1;

  prev.onclick = Previous;
  next.onclick = Next;
});
