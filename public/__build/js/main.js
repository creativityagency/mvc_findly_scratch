(function(){function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s}return e})()({1:[function(require,module,exports){
var prev, next, slides, position;

function Previous() {
  if (position > 1) {
    position -= 1;
    for (var i = 0; i < slides.length; i++) {
      slides[i].style.transform = 'translateX(-' + (position - 1) * 21 + 'vw)';
    }
  } else if (position == 1) {
    for (var j = 0; j < slides.length; j++) {
      slides[j].style.transform = 'translateX(0vw)';
    }
  }
}

function Next() {
  if (position < slides.length - 2) {
    position += 1;
    for (var i = 0; i < slides.length; i++) {
      slides[i].style.transform = 'translateX(-' + (position - 1) * 21 + 'vw)';
    }
  }
}

window.addEventListener('DOMContentLoaded', function () {
  prev = document.querySelector('#prev');
  next = document.querySelector('#next');
  slides = document.querySelectorAll('#slider > .slide');
  position = 1;

  prev.onclick = Previous;
  next.onclick = Next;
});

},{}],2:[function(require,module,exports){
require('./validate');

require('./carousel');

},{"./carousel":1,"./validate":3}],3:[function(require,module,exports){
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*
 * validate.js 2.0.1
 * Copyright (c) 2011 - 2015 Rick Harrison, http://rickharrison.me
 * validate.js is open sourced under the MIT license.
 * Portions of validate.js are inspired by CodeIgniter.
 * http://rickharrison.github.com/validate.js
 */

(function (window, document, undefined) {
    /*
     * If you would like an application-wide config, change these defaults.
     * Otherwise, use the setMessage() function to configure form specific messages.
     */

    var defaults = {
        messages: {
            required: 'The %s field is required.',
            matches: 'The %s field does not match the %s field.',
            "default": 'The %s field is still set to default, please change.',
            valid_email: 'The %s field must contain a valid email address.',
            valid_emails: 'The %s field must contain all valid email addresses.',
            min_length: 'The %s field must be at least %s characters in length.',
            max_length: 'The %s field must not exceed %s characters in length.',
            exact_length: 'The %s field must be exactly %s characters in length.',
            greater_than: 'The %s field must contain a number greater than %s.',
            less_than: 'The %s field must contain a number less than %s.',
            alpha: 'The %s field must only contain alphabetical characters.',
            alpha_numeric: 'The %s field must only contain alpha-numeric characters.',
            alpha_dash: 'The %s field must only contain alpha-numeric characters, underscores, and dashes.',
            numeric: 'The %s field must contain only numbers.',
            integer: 'The %s field must contain an integer.',
            decimal: 'The %s field must contain a decimal number.',
            is_natural: 'The %s field must contain only positive numbers.',
            is_natural_no_zero: 'The %s field must contain a number greater than zero.',
            valid_ip: 'The %s field must contain a valid IP.',
            valid_base64: 'The %s field must contain a base64 string.',
            valid_credit_card: 'The %s field must contain a valid credit card number.',
            is_file_type: 'The %s field must contain only %s files.',
            valid_url: 'The %s field must contain a valid URL.',
            greater_than_date: 'The %s field must contain a more recent date than %s.',
            less_than_date: 'The %s field must contain an older date than %s.',
            greater_than_or_equal_date: 'The %s field must contain a date that\'s at least as recent as %s.',
            less_than_or_equal_date: 'The %s field must contain a date that\'s %s or older.'
        },
        callback: function () {
            function callback(errors) {}

            return callback;
        }()
    };

    /*
     * Define the regular expressions that will be used
     */

    var ruleRegex = /^(.+?)\[(.+)\]$/,
        numericRegex = /^[0-9]+$/,
        integerRegex = /^\-?[0-9]+$/,
        decimalRegex = /^\-?[0-9]*\.?[0-9]+$/,
        emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
        alphaRegex = /^[a-z]+$/i,
        alphaNumericRegex = /^[a-z0-9]+$/i,
        alphaDashRegex = /^[a-z0-9_\-]+$/i,
        naturalRegex = /^[0-9]+$/i,
        naturalNoZeroRegex = /^[1-9][0-9]*$/i,
        ipRegex = /^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})$/i,
        base64Regex = /[^a-zA-Z0-9\/\+=]/i,
        numericDashRegex = /^[\d\-\s]+$/,
        urlRegex = /^((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/,
        dateRegex = /\d{4}-\d{1,2}-\d{1,2}/;

    /*
     * The exposed public object to validate a form:
     *
     * @param formNameOrNode - String - The name attribute of the form (i.e. <form name="myForm"></form>) or node of the form element
     * @param fields - Array - [{
     *     name: The name of the element (i.e. <input name="myField" />)
     *     display: 'Field Name'
     *     rules: required|matches[password_confirm]
     * }]
     * @param callback - Function - The callback after validation has been performed.
     *     @argument errors - An array of validation errors
     *     @argument event - The javascript event
     */

    var FormValidator = function () {
        function FormValidator(formNameOrNode, fields, callback) {
            this.callback = callback || defaults.callback;
            this.errors = [];
            this.fields = {};
            this.form = this._formByNameOrNode(formNameOrNode) || {};
            this.messages = {};
            this.handlers = {};
            this.conditionals = {};

            for (var i = 0, fieldLength = fields.length; i < fieldLength; i++) {
                var field = fields[i];

                // If passed in incorrectly, we need to skip the field.
                if (!field.name && !field.names || !field.rules) {
                    console.warn('validate.js: The following field is being skipped due to a misconfiguration:');
                    console.warn(field);
                    console.warn('Check to ensure you have properly configured a name and rules for this field');
                    continue;
                }

                /*
                 * Build the master fields array that has all the information needed to validate
                 */

                if (field.names) {
                    for (var j = 0, fieldNamesLength = field.names.length; j < fieldNamesLength; j++) {
                        this._addField(field, field.names[j]);
                    }
                } else {
                    this._addField(field, field.name);
                }
            }

            /*
             * Attach an event callback for the form submission
             */

            var _onsubmit = this.form.onsubmit;

            this.form.onsubmit = function (that) {
                return function (evt) {
                    try {
                        return that._validateForm(evt) && (_onsubmit === undefined || _onsubmit());
                    } catch (e) {}
                };
            }(this);
        }

        return FormValidator;
    }(),
        attributeValue = function () {
        function attributeValue(element, attributeName) {
            var i;

            if (element.length > 0 && (element[0].type === 'radio' || element[0].type === 'checkbox')) {
                for (i = 0, elementLength = element.length; i < elementLength; i++) {
                    if (element[i].checked) {
                        return element[i][attributeName];
                    }
                }

                return;
            }

            return element[attributeName];
        }

        return attributeValue;
    }();

    /*
     * @public
     * Sets a custom message for one of the rules
     */

    FormValidator.prototype.setMessage = function (rule, message) {
        this.messages[rule] = message;

        // return this for chaining
        return this;
    };

    /*
     * @public
     * Registers a callback for a custom rule (i.e. callback_username_check)
     */

    FormValidator.prototype.registerCallback = function (name, handler) {
        if (name && typeof name === 'string' && handler && typeof handler === 'function') {
            this.handlers[name] = handler;
        }

        // return this for chaining
        return this;
    };

    /*
     * @public
     * Registers a conditional for a custom 'depends' rule
     */

    FormValidator.prototype.registerConditional = function (name, conditional) {
        if (name && typeof name === 'string' && conditional && typeof conditional === 'function') {
            this.conditionals[name] = conditional;
        }

        // return this for chaining
        return this;
    };

    /*
     * @private
     * Determines if a form dom node was passed in or just a string representing the form name
     */

    FormValidator.prototype._formByNameOrNode = function (formNameOrNode) {
        return (typeof formNameOrNode === 'undefined' ? 'undefined' : _typeof(formNameOrNode)) === 'object' ? formNameOrNode : document.forms[formNameOrNode];
    };

    /*
     * @private
     * Adds a file to the master fields array
     */

    FormValidator.prototype._addField = function (field, nameValue) {
        this.fields[nameValue] = {
            name: nameValue,
            display: field.display || nameValue,
            rules: field.rules,
            depends: field.depends,
            id: null,
            element: null,
            type: null,
            value: null,
            checked: null
        };
    };

    /*
     * @private
     * Runs the validation when the form is submitted.
     */

    FormValidator.prototype._validateForm = function (evt) {
        this.errors = [];

        for (var key in this.fields) {
            if (this.fields.hasOwnProperty(key)) {
                var field = this.fields[key] || {},
                    element = this.form[field.name];

                if (element && element !== undefined) {
                    field.id = attributeValue(element, 'id');
                    field.element = element;
                    field.type = element.length > 0 ? element[0].type : element.type;
                    field.value = attributeValue(element, 'value');
                    field.checked = attributeValue(element, 'checked');

                    /*
                     * Run through the rules for each field.
                     * If the field has a depends conditional, only validate the field
                     * if it passes the custom function
                     */

                    if (field.depends && typeof field.depends === "function") {
                        if (field.depends.call(this, field)) {
                            this._validateField(field);
                        }
                    } else if (field.depends && typeof field.depends === "string" && this.conditionals[field.depends]) {
                        if (this.conditionals[field.depends].call(this, field)) {
                            this._validateField(field);
                        }
                    } else {
                        this._validateField(field);
                    }
                }
            }
        }

        if (typeof this.callback === 'function') {
            this.callback(this.errors, evt);
        }

        if (this.errors.length > 0) {
            if (evt && evt.preventDefault) {
                evt.preventDefault();
            } else if (event) {
                // IE uses the global event variable
                event.returnValue = false;
            }
        }

        return true;
    };

    /*
     * @private
     * Looks at the fields value and evaluates it against the given rules
     */

    FormValidator.prototype._validateField = function (field) {
        var i,
            j,
            rules = field.rules.split('|'),
            indexOfRequired = field.rules.indexOf('required'),
            isEmpty = !field.value || field.value === '' || field.value === undefined;

        /*
         * Run through the rules and execute the validation methods as needed
         */

        for (i = 0, ruleLength = rules.length; i < ruleLength; i++) {
            var method = rules[i],
                param = null,
                failed = false,
                parts = ruleRegex.exec(method);

            /*
             * If this field is not required and the value is empty, continue on to the next rule unless it's a callback.
             * This ensures that a callback will always be called but other rules will be skipped.
             */

            if (indexOfRequired === -1 && method.indexOf('!callback_') === -1 && isEmpty) {
                continue;
            }

            /*
             * If the rule has a parameter (i.e. matches[param]) split it out
             */

            if (parts) {
                method = parts[1];
                param = parts[2];
            }

            if (method.charAt(0) === '!') {
                method = method.substring(1, method.length);
            }

            /*
             * If the hook is defined, run it to find any validation errors
             */

            if (typeof this._hooks[method] === 'function') {
                if (!this._hooks[method].apply(this, [field, param])) {
                    failed = true;
                }
            } else if (method.substring(0, 9) === 'callback_') {
                // Custom method. Execute the handler if it was registered
                method = method.substring(9, method.length);

                if (typeof this.handlers[method] === 'function') {
                    if (this.handlers[method].apply(this, [field.value, param, field]) === false) {
                        failed = true;
                    }
                }
            }

            /*
             * If the hook failed, add a message to the errors array
             */

            if (failed) {
                // Make sure we have a message for this rule
                var source = this.messages[field.name + '.' + method] || this.messages[method] || defaults.messages[method],
                    message = 'An error has occurred with the ' + field.display + ' field.';

                if (source) {
                    message = source.replace('%s', field.display);

                    if (param) {
                        message = message.replace('%s', this.fields[param] ? this.fields[param].display : param);
                    }
                }

                var existingError;
                for (j = 0; j < this.errors.length; j += 1) {
                    if (field.id === this.errors[j].id) {
                        existingError = this.errors[j];
                    }
                }

                var errorObject = existingError || {
                    id: field.id,
                    display: field.display,
                    element: field.element,
                    name: field.name,
                    message: message,
                    messages: [],
                    rule: method
                };
                errorObject.messages.push(message);
                if (!existingError) this.errors.push(errorObject);
            }
        }
    };

    /**
     * private function _getValidDate: helper function to convert a string date to a Date object
     * @param date (String) must be in format yyyy-mm-dd or use keyword: today
     * @returns {Date} returns false if invalid
     */
    FormValidator.prototype._getValidDate = function (date) {
        if (!date.match('today') && !date.match(dateRegex)) {
            return false;
        }

        var validDate = new Date(),
            validDateArray;

        if (!date.match('today')) {
            validDateArray = date.split('-');
            validDate.setFullYear(validDateArray[0]);
            validDate.setMonth(validDateArray[1] - 1);
            validDate.setDate(validDateArray[2]);
        }
        return validDate;
    };

    /*
     * @private
     * Object containing all of the validation hooks
     */

    FormValidator.prototype._hooks = {
        required: function () {
            function required(field) {
                var value = field.value;

                if (field.type === 'checkbox' || field.type === 'radio') {
                    return field.checked === true;
                }

                return value !== null && value !== '';
            }

            return required;
        }(),

        "default": function () {
            function _default(field, defaultName) {
                return field.value !== defaultName;
            }

            return _default;
        }(),

        matches: function () {
            function matches(field, matchName) {
                var el = this.form[matchName];

                if (el) {
                    return field.value === el.value;
                }

                return false;
            }

            return matches;
        }(),

        valid_email: function () {
            function valid_email(field) {
                return emailRegex.test(field.value);
            }

            return valid_email;
        }(),

        valid_emails: function () {
            function valid_emails(field) {
                var result = field.value.split(/\s*,\s*/g);

                for (var i = 0, resultLength = result.length; i < resultLength; i++) {
                    if (!emailRegex.test(result[i])) {
                        return false;
                    }
                }

                return true;
            }

            return valid_emails;
        }(),

        min_length: function () {
            function min_length(field, length) {
                if (!numericRegex.test(length)) {
                    return false;
                }

                return field.value.length >= parseInt(length, 10);
            }

            return min_length;
        }(),

        max_length: function () {
            function max_length(field, length) {
                if (!numericRegex.test(length)) {
                    return false;
                }

                return field.value.length <= parseInt(length, 10);
            }

            return max_length;
        }(),

        exact_length: function () {
            function exact_length(field, length) {
                if (!numericRegex.test(length)) {
                    return false;
                }

                return field.value.length === parseInt(length, 10);
            }

            return exact_length;
        }(),

        greater_than: function () {
            function greater_than(field, param) {
                if (!decimalRegex.test(field.value)) {
                    return false;
                }

                return parseFloat(field.value) > parseFloat(param);
            }

            return greater_than;
        }(),

        less_than: function () {
            function less_than(field, param) {
                if (!decimalRegex.test(field.value)) {
                    return false;
                }

                return parseFloat(field.value) < parseFloat(param);
            }

            return less_than;
        }(),

        alpha: function () {
            function alpha(field) {
                return alphaRegex.test(field.value);
            }

            return alpha;
        }(),

        alpha_numeric: function () {
            function alpha_numeric(field) {
                return alphaNumericRegex.test(field.value);
            }

            return alpha_numeric;
        }(),

        alpha_dash: function () {
            function alpha_dash(field) {
                return alphaDashRegex.test(field.value);
            }

            return alpha_dash;
        }(),

        numeric: function () {
            function numeric(field) {
                return numericRegex.test(field.value);
            }

            return numeric;
        }(),

        integer: function () {
            function integer(field) {
                return integerRegex.test(field.value);
            }

            return integer;
        }(),

        decimal: function () {
            function decimal(field) {
                return decimalRegex.test(field.value);
            }

            return decimal;
        }(),

        is_natural: function () {
            function is_natural(field) {
                return naturalRegex.test(field.value);
            }

            return is_natural;
        }(),

        is_natural_no_zero: function () {
            function is_natural_no_zero(field) {
                return naturalNoZeroRegex.test(field.value);
            }

            return is_natural_no_zero;
        }(),

        valid_ip: function () {
            function valid_ip(field) {
                return ipRegex.test(field.value);
            }

            return valid_ip;
        }(),

        valid_base64: function () {
            function valid_base64(field) {
                return base64Regex.test(field.value);
            }

            return valid_base64;
        }(),

        valid_url: function () {
            function valid_url(field) {
                return urlRegex.test(field.value);
            }

            return valid_url;
        }(),

        valid_credit_card: function () {
            function valid_credit_card(field) {
                // Luhn Check Code from https://gist.github.com/4075533
                // accept only digits, dashes or spaces
                if (!numericDashRegex.test(field.value)) return false;

                // The Luhn Algorithm. It's so pretty.
                var nCheck = 0,
                    nDigit = 0,
                    bEven = false;
                var strippedField = field.value.replace(/\D/g, "");

                for (var n = strippedField.length - 1; n >= 0; n--) {
                    var cDigit = strippedField.charAt(n);
                    nDigit = parseInt(cDigit, 10);
                    if (bEven) {
                        if ((nDigit *= 2) > 9) nDigit -= 9;
                    }

                    nCheck += nDigit;
                    bEven = !bEven;
                }

                return nCheck % 10 === 0;
            }

            return valid_credit_card;
        }(),

        is_file_type: function () {
            function is_file_type(field, type) {
                if (field.type !== 'file') {
                    return true;
                }

                var ext = field.value.substr(field.value.lastIndexOf('.') + 1),
                    typeArray = type.split(','),
                    inArray = false,
                    i = 0,
                    len = typeArray.length;

                for (i; i < len; i++) {
                    if (ext.toUpperCase() == typeArray[i].toUpperCase()) inArray = true;
                }

                return inArray;
            }

            return is_file_type;
        }(),

        greater_than_date: function () {
            function greater_than_date(field, date) {
                var enteredDate = this._getValidDate(field.value),
                    validDate = this._getValidDate(date);

                if (!validDate || !enteredDate) {
                    return false;
                }

                return enteredDate > validDate;
            }

            return greater_than_date;
        }(),

        less_than_date: function () {
            function less_than_date(field, date) {
                var enteredDate = this._getValidDate(field.value),
                    validDate = this._getValidDate(date);

                if (!validDate || !enteredDate) {
                    return false;
                }

                return enteredDate < validDate;
            }

            return less_than_date;
        }(),

        greater_than_or_equal_date: function () {
            function greater_than_or_equal_date(field, date) {
                var enteredDate = this._getValidDate(field.value),
                    validDate = this._getValidDate(date);

                if (!validDate || !enteredDate) {
                    return false;
                }

                return enteredDate >= validDate;
            }

            return greater_than_or_equal_date;
        }(),

        less_than_or_equal_date: function () {
            function less_than_or_equal_date(field, date) {
                var enteredDate = this._getValidDate(field.value),
                    validDate = this._getValidDate(date);

                if (!validDate || !enteredDate) {
                    return false;
                }

                return enteredDate <= validDate;
            }

            return less_than_or_equal_date;
        }()
    };

    window.FormValidator = FormValidator;
})(window, document);

/*
 * Export as a CommonJS module
 */
if (typeof module !== 'undefined' && module.exports) {
    module.exports = FormValidator;
}

},{}]},{},[2])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJwdWJsaWMvc3JjL2pzL2Nhcm91c2VsLmpzIiwicHVibGljL3NyYy9qcy9tYWluLmpzIiwicHVibGljL3NyYy9qcy92YWxpZGF0ZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBLElBQUksSUFBSixFQUFVLElBQVYsRUFBZ0IsTUFBaEIsRUFBd0IsUUFBeEI7O0FBRUEsU0FBUyxRQUFULEdBQW9CO0FBQ2xCLE1BQUksV0FBVyxDQUFmLEVBQWtCO0FBQ2hCLGdCQUFZLENBQVo7QUFDQSxTQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksT0FBTyxNQUEzQixFQUFtQyxHQUFuQyxFQUF3QztBQUN0QyxhQUFPLENBQVAsRUFBVSxLQUFWLENBQWdCLFNBQWhCLEdBQTRCLGlCQUFrQixDQUFDLFdBQVcsQ0FBWixJQUFpQixFQUFuQyxHQUF5QyxLQUFyRTtBQUNEO0FBQ0YsR0FMRCxNQUtPLElBQUksWUFBWSxDQUFoQixFQUFtQjtBQUN4QixTQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksT0FBTyxNQUEzQixFQUFtQyxHQUFuQyxFQUF3QztBQUN0QyxhQUFPLENBQVAsRUFBVSxLQUFWLENBQWdCLFNBQWhCLEdBQTRCLGlCQUE1QjtBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxTQUFTLElBQVQsR0FBZ0I7QUFDZCxNQUFJLFdBQVksT0FBTyxNQUFQLEdBQWdCLENBQWhDLEVBQW9DO0FBQ2xDLGdCQUFZLENBQVo7QUFDQSxTQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksT0FBTyxNQUEzQixFQUFtQyxHQUFuQyxFQUF3QztBQUN0QyxhQUFPLENBQVAsRUFBVSxLQUFWLENBQWdCLFNBQWhCLEdBQTRCLGlCQUFrQixDQUFDLFdBQVcsQ0FBWixJQUFpQixFQUFuQyxHQUF5QyxLQUFyRTtBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxPQUFPLGdCQUFQLENBQXdCLGtCQUF4QixFQUE0QyxZQUFZO0FBQ3RELFNBQU8sU0FBUyxhQUFULENBQXVCLE9BQXZCLENBQVA7QUFDQSxTQUFPLFNBQVMsYUFBVCxDQUF1QixPQUF2QixDQUFQO0FBQ0EsV0FBUyxTQUFTLGdCQUFULENBQTBCLGtCQUExQixDQUFUO0FBQ0EsYUFBVyxDQUFYOztBQUVBLE9BQUssT0FBTCxHQUFlLFFBQWY7QUFDQSxPQUFLLE9BQUwsR0FBZSxJQUFmO0FBQ0QsQ0FSRDs7O0FDeEJBOztBQUNBOzs7OztBQ0RBOzs7Ozs7OztBQVFBLENBQUMsVUFBUyxNQUFULEVBQWlCLFFBQWpCLEVBQTJCLFNBQTNCLEVBQXNDO0FBQ25DOzs7OztBQUtBLFFBQUksV0FBVztBQUNYLGtCQUFVO0FBQ04sc0JBQVUsMkJBREo7QUFFTixxQkFBUywyQ0FGSDtBQUdOLHVCQUFXLHNEQUhMO0FBSU4seUJBQWEsa0RBSlA7QUFLTiwwQkFBYyxzREFMUjtBQU1OLHdCQUFZLHdEQU5OO0FBT04sd0JBQVksdURBUE47QUFRTiwwQkFBYyx1REFSUjtBQVNOLDBCQUFjLHFEQVRSO0FBVU4sdUJBQVcsa0RBVkw7QUFXTixtQkFBTyx5REFYRDtBQVlOLDJCQUFlLDBEQVpUO0FBYU4sd0JBQVksbUZBYk47QUFjTixxQkFBUyx5Q0FkSDtBQWVOLHFCQUFTLHVDQWZIO0FBZ0JOLHFCQUFTLDZDQWhCSDtBQWlCTix3QkFBWSxrREFqQk47QUFrQk4sZ0NBQW9CLHVEQWxCZDtBQW1CTixzQkFBVSx1Q0FuQko7QUFvQk4sMEJBQWMsNENBcEJSO0FBcUJOLCtCQUFtQix1REFyQmI7QUFzQk4sMEJBQWMsMENBdEJSO0FBdUJOLHVCQUFXLHdDQXZCTDtBQXdCTiwrQkFBbUIsdURBeEJiO0FBeUJOLDRCQUFnQixrREF6QlY7QUEwQk4sd0NBQTRCLG9FQTFCdEI7QUEyQk4scUNBQXlCO0FBM0JuQixTQURDO0FBOEJYO0FBQVUsOEJBQVMsTUFBVCxFQUFpQixDQUUxQjs7QUFGRDtBQUFBO0FBOUJXLEtBQWY7O0FBbUNBOzs7O0FBSUEsUUFBSSxZQUFZLGlCQUFoQjtBQUFBLFFBQ0ksZUFBZSxVQURuQjtBQUFBLFFBRUksZUFBZSxhQUZuQjtBQUFBLFFBR0ksZUFBZSxzQkFIbkI7QUFBQSxRQUlJLGFBQWEsc0lBSmpCO0FBQUEsUUFLSSxhQUFhLFdBTGpCO0FBQUEsUUFNSSxvQkFBb0IsY0FOeEI7QUFBQSxRQU9JLGlCQUFpQixpQkFQckI7QUFBQSxRQVFJLGVBQWUsV0FSbkI7QUFBQSxRQVNJLHFCQUFxQixnQkFUekI7QUFBQSxRQVVJLFVBQVUsZ0dBVmQ7QUFBQSxRQVdJLGNBQWMsb0JBWGxCO0FBQUEsUUFZSSxtQkFBbUIsYUFadkI7QUFBQSxRQWFJLFdBQVcsb0ZBYmY7QUFBQSxRQWNJLFlBQVksdUJBZGhCOztBQWdCQTs7Ozs7Ozs7Ozs7Ozs7QUFjQSxRQUFJO0FBQWdCLGlCQUFoQixhQUFnQixDQUFTLGNBQVQsRUFBeUIsTUFBekIsRUFBaUMsUUFBakMsRUFBMkM7QUFDM0QsaUJBQUssUUFBTCxHQUFnQixZQUFZLFNBQVMsUUFBckM7QUFDQSxpQkFBSyxNQUFMLEdBQWMsRUFBZDtBQUNBLGlCQUFLLE1BQUwsR0FBYyxFQUFkO0FBQ0EsaUJBQUssSUFBTCxHQUFZLEtBQUssaUJBQUwsQ0FBdUIsY0FBdkIsS0FBMEMsRUFBdEQ7QUFDQSxpQkFBSyxRQUFMLEdBQWdCLEVBQWhCO0FBQ0EsaUJBQUssUUFBTCxHQUFnQixFQUFoQjtBQUNBLGlCQUFLLFlBQUwsR0FBb0IsRUFBcEI7O0FBRUEsaUJBQUssSUFBSSxJQUFJLENBQVIsRUFBVyxjQUFjLE9BQU8sTUFBckMsRUFBNkMsSUFBSSxXQUFqRCxFQUE4RCxHQUE5RCxFQUFtRTtBQUMvRCxvQkFBSSxRQUFRLE9BQU8sQ0FBUCxDQUFaOztBQUVBO0FBQ0Esb0JBQUssQ0FBQyxNQUFNLElBQVAsSUFBZSxDQUFDLE1BQU0sS0FBdkIsSUFBaUMsQ0FBQyxNQUFNLEtBQTVDLEVBQW1EO0FBQy9DLDRCQUFRLElBQVIsQ0FBYSw4RUFBYjtBQUNBLDRCQUFRLElBQVIsQ0FBYSxLQUFiO0FBQ0EsNEJBQVEsSUFBUixDQUFhLDhFQUFiO0FBQ0E7QUFDSDs7QUFFRDs7OztBQUlBLG9CQUFJLE1BQU0sS0FBVixFQUFpQjtBQUNiLHlCQUFLLElBQUksSUFBSSxDQUFSLEVBQVcsbUJBQW1CLE1BQU0sS0FBTixDQUFZLE1BQS9DLEVBQXVELElBQUksZ0JBQTNELEVBQTZFLEdBQTdFLEVBQWtGO0FBQzlFLDZCQUFLLFNBQUwsQ0FBZSxLQUFmLEVBQXNCLE1BQU0sS0FBTixDQUFZLENBQVosQ0FBdEI7QUFDSDtBQUNKLGlCQUpELE1BSU87QUFDSCx5QkFBSyxTQUFMLENBQWUsS0FBZixFQUFzQixNQUFNLElBQTVCO0FBQ0g7QUFDSjs7QUFFRDs7OztBQUlBLGdCQUFJLFlBQVksS0FBSyxJQUFMLENBQVUsUUFBMUI7O0FBRUEsaUJBQUssSUFBTCxDQUFVLFFBQVYsR0FBc0IsVUFBUyxJQUFULEVBQWU7QUFDakMsdUJBQU8sVUFBUyxHQUFULEVBQWM7QUFDakIsd0JBQUk7QUFDQSwrQkFBTyxLQUFLLGFBQUwsQ0FBbUIsR0FBbkIsTUFBNEIsY0FBYyxTQUFkLElBQTJCLFdBQXZELENBQVA7QUFDSCxxQkFGRCxDQUVFLE9BQU0sQ0FBTixFQUFTLENBQUU7QUFDaEIsaUJBSkQ7QUFLSCxhQU5vQixDQU1sQixJQU5rQixDQUFyQjtBQU9IOztBQTlDRztBQUFBLE9BQUo7QUFBQSxRQWdEQTtBQUFpQixpQkFBakIsY0FBaUIsQ0FBVSxPQUFWLEVBQW1CLGFBQW5CLEVBQWtDO0FBQy9DLGdCQUFJLENBQUo7O0FBRUEsZ0JBQUssUUFBUSxNQUFSLEdBQWlCLENBQWxCLEtBQXlCLFFBQVEsQ0FBUixFQUFXLElBQVgsS0FBb0IsT0FBcEIsSUFBK0IsUUFBUSxDQUFSLEVBQVcsSUFBWCxLQUFvQixVQUE1RSxDQUFKLEVBQTZGO0FBQ3pGLHFCQUFLLElBQUksQ0FBSixFQUFPLGdCQUFnQixRQUFRLE1BQXBDLEVBQTRDLElBQUksYUFBaEQsRUFBK0QsR0FBL0QsRUFBb0U7QUFDaEUsd0JBQUksUUFBUSxDQUFSLEVBQVcsT0FBZixFQUF3QjtBQUNwQiwrQkFBTyxRQUFRLENBQVIsRUFBVyxhQUFYLENBQVA7QUFDSDtBQUNKOztBQUVEO0FBQ0g7O0FBRUQsbUJBQU8sUUFBUSxhQUFSLENBQVA7QUFDSDs7QUFkRDtBQUFBLE9BaERBOztBQWdFQTs7Ozs7QUFLQSxrQkFBYyxTQUFkLENBQXdCLFVBQXhCLEdBQXFDLFVBQVMsSUFBVCxFQUFlLE9BQWYsRUFBd0I7QUFDekQsYUFBSyxRQUFMLENBQWMsSUFBZCxJQUFzQixPQUF0Qjs7QUFFQTtBQUNBLGVBQU8sSUFBUDtBQUNILEtBTEQ7O0FBT0E7Ozs7O0FBS0Esa0JBQWMsU0FBZCxDQUF3QixnQkFBeEIsR0FBMkMsVUFBUyxJQUFULEVBQWUsT0FBZixFQUF3QjtBQUMvRCxZQUFJLFFBQVEsT0FBTyxJQUFQLEtBQWdCLFFBQXhCLElBQW9DLE9BQXBDLElBQStDLE9BQU8sT0FBUCxLQUFtQixVQUF0RSxFQUFrRjtBQUM5RSxpQkFBSyxRQUFMLENBQWMsSUFBZCxJQUFzQixPQUF0QjtBQUNIOztBQUVEO0FBQ0EsZUFBTyxJQUFQO0FBQ0gsS0FQRDs7QUFTQTs7Ozs7QUFLQSxrQkFBYyxTQUFkLENBQXdCLG1CQUF4QixHQUE4QyxVQUFTLElBQVQsRUFBZSxXQUFmLEVBQTRCO0FBQ3RFLFlBQUksUUFBUSxPQUFPLElBQVAsS0FBZ0IsUUFBeEIsSUFBb0MsV0FBcEMsSUFBbUQsT0FBTyxXQUFQLEtBQXVCLFVBQTlFLEVBQTBGO0FBQ3RGLGlCQUFLLFlBQUwsQ0FBa0IsSUFBbEIsSUFBMEIsV0FBMUI7QUFDSDs7QUFFRDtBQUNBLGVBQU8sSUFBUDtBQUNILEtBUEQ7O0FBU0E7Ozs7O0FBS0Esa0JBQWMsU0FBZCxDQUF3QixpQkFBeEIsR0FBNEMsVUFBUyxjQUFULEVBQXlCO0FBQ2pFLGVBQVEsUUFBTyxjQUFQLHlDQUFPLGNBQVAsT0FBMEIsUUFBM0IsR0FBdUMsY0FBdkMsR0FBd0QsU0FBUyxLQUFULENBQWUsY0FBZixDQUEvRDtBQUNILEtBRkQ7O0FBSUE7Ozs7O0FBS0Esa0JBQWMsU0FBZCxDQUF3QixTQUF4QixHQUFvQyxVQUFTLEtBQVQsRUFBZ0IsU0FBaEIsRUFBNEI7QUFDNUQsYUFBSyxNQUFMLENBQVksU0FBWixJQUF5QjtBQUNyQixrQkFBTSxTQURlO0FBRXJCLHFCQUFTLE1BQU0sT0FBTixJQUFpQixTQUZMO0FBR3JCLG1CQUFPLE1BQU0sS0FIUTtBQUlyQixxQkFBUyxNQUFNLE9BSk07QUFLckIsZ0JBQUksSUFMaUI7QUFNckIscUJBQVMsSUFOWTtBQU9yQixrQkFBTSxJQVBlO0FBUXJCLG1CQUFPLElBUmM7QUFTckIscUJBQVM7QUFUWSxTQUF6QjtBQVdILEtBWkQ7O0FBY0E7Ozs7O0FBS0Esa0JBQWMsU0FBZCxDQUF3QixhQUF4QixHQUF3QyxVQUFTLEdBQVQsRUFBYztBQUNsRCxhQUFLLE1BQUwsR0FBYyxFQUFkOztBQUVBLGFBQUssSUFBSSxHQUFULElBQWdCLEtBQUssTUFBckIsRUFBNkI7QUFDekIsZ0JBQUksS0FBSyxNQUFMLENBQVksY0FBWixDQUEyQixHQUEzQixDQUFKLEVBQXFDO0FBQ2pDLG9CQUFJLFFBQVEsS0FBSyxNQUFMLENBQVksR0FBWixLQUFvQixFQUFoQztBQUFBLG9CQUNJLFVBQVUsS0FBSyxJQUFMLENBQVUsTUFBTSxJQUFoQixDQURkOztBQUdBLG9CQUFJLFdBQVcsWUFBWSxTQUEzQixFQUFzQztBQUNsQywwQkFBTSxFQUFOLEdBQVcsZUFBZSxPQUFmLEVBQXdCLElBQXhCLENBQVg7QUFDQSwwQkFBTSxPQUFOLEdBQWdCLE9BQWhCO0FBQ0EsMEJBQU0sSUFBTixHQUFjLFFBQVEsTUFBUixHQUFpQixDQUFsQixHQUF1QixRQUFRLENBQVIsRUFBVyxJQUFsQyxHQUF5QyxRQUFRLElBQTlEO0FBQ0EsMEJBQU0sS0FBTixHQUFjLGVBQWUsT0FBZixFQUF3QixPQUF4QixDQUFkO0FBQ0EsMEJBQU0sT0FBTixHQUFnQixlQUFlLE9BQWYsRUFBd0IsU0FBeEIsQ0FBaEI7O0FBRUE7Ozs7OztBQU1BLHdCQUFJLE1BQU0sT0FBTixJQUFpQixPQUFPLE1BQU0sT0FBYixLQUF5QixVQUE5QyxFQUEwRDtBQUN0RCw0QkFBSSxNQUFNLE9BQU4sQ0FBYyxJQUFkLENBQW1CLElBQW5CLEVBQXlCLEtBQXpCLENBQUosRUFBcUM7QUFDakMsaUNBQUssY0FBTCxDQUFvQixLQUFwQjtBQUNIO0FBQ0oscUJBSkQsTUFJTyxJQUFJLE1BQU0sT0FBTixJQUFpQixPQUFPLE1BQU0sT0FBYixLQUF5QixRQUExQyxJQUFzRCxLQUFLLFlBQUwsQ0FBa0IsTUFBTSxPQUF4QixDQUExRCxFQUE0RjtBQUMvRiw0QkFBSSxLQUFLLFlBQUwsQ0FBa0IsTUFBTSxPQUF4QixFQUFpQyxJQUFqQyxDQUFzQyxJQUF0QyxFQUEyQyxLQUEzQyxDQUFKLEVBQXVEO0FBQ25ELGlDQUFLLGNBQUwsQ0FBb0IsS0FBcEI7QUFDSDtBQUNKLHFCQUpNLE1BSUE7QUFDSCw2QkFBSyxjQUFMLENBQW9CLEtBQXBCO0FBQ0g7QUFDSjtBQUNKO0FBQ0o7O0FBRUQsWUFBSSxPQUFPLEtBQUssUUFBWixLQUF5QixVQUE3QixFQUF5QztBQUNyQyxpQkFBSyxRQUFMLENBQWMsS0FBSyxNQUFuQixFQUEyQixHQUEzQjtBQUNIOztBQUVELFlBQUksS0FBSyxNQUFMLENBQVksTUFBWixHQUFxQixDQUF6QixFQUE0QjtBQUN4QixnQkFBSSxPQUFPLElBQUksY0FBZixFQUErQjtBQUMzQixvQkFBSSxjQUFKO0FBQ0gsYUFGRCxNQUVPLElBQUksS0FBSixFQUFXO0FBQ2Q7QUFDQSxzQkFBTSxXQUFOLEdBQW9CLEtBQXBCO0FBQ0g7QUFDSjs7QUFFRCxlQUFPLElBQVA7QUFDSCxLQWxERDs7QUFvREE7Ozs7O0FBS0Esa0JBQWMsU0FBZCxDQUF3QixjQUF4QixHQUF5QyxVQUFTLEtBQVQsRUFBZ0I7QUFDckQsWUFBSSxDQUFKO0FBQUEsWUFBTyxDQUFQO0FBQUEsWUFDSSxRQUFRLE1BQU0sS0FBTixDQUFZLEtBQVosQ0FBa0IsR0FBbEIsQ0FEWjtBQUFBLFlBRUksa0JBQWtCLE1BQU0sS0FBTixDQUFZLE9BQVosQ0FBb0IsVUFBcEIsQ0FGdEI7QUFBQSxZQUdJLFVBQVcsQ0FBQyxNQUFNLEtBQVAsSUFBZ0IsTUFBTSxLQUFOLEtBQWdCLEVBQWhDLElBQXNDLE1BQU0sS0FBTixLQUFnQixTQUhyRTs7QUFLQTs7OztBQUlBLGFBQUssSUFBSSxDQUFKLEVBQU8sYUFBYSxNQUFNLE1BQS9CLEVBQXVDLElBQUksVUFBM0MsRUFBdUQsR0FBdkQsRUFBNEQ7QUFDeEQsZ0JBQUksU0FBUyxNQUFNLENBQU4sQ0FBYjtBQUFBLGdCQUNJLFFBQVEsSUFEWjtBQUFBLGdCQUVJLFNBQVMsS0FGYjtBQUFBLGdCQUdJLFFBQVEsVUFBVSxJQUFWLENBQWUsTUFBZixDQUhaOztBQUtBOzs7OztBQUtBLGdCQUFJLG9CQUFvQixDQUFDLENBQXJCLElBQTBCLE9BQU8sT0FBUCxDQUFlLFlBQWYsTUFBaUMsQ0FBQyxDQUE1RCxJQUFpRSxPQUFyRSxFQUE4RTtBQUMxRTtBQUNIOztBQUVEOzs7O0FBSUEsZ0JBQUksS0FBSixFQUFXO0FBQ1AseUJBQVMsTUFBTSxDQUFOLENBQVQ7QUFDQSx3QkFBUSxNQUFNLENBQU4sQ0FBUjtBQUNIOztBQUVELGdCQUFJLE9BQU8sTUFBUCxDQUFjLENBQWQsTUFBcUIsR0FBekIsRUFBOEI7QUFDMUIseUJBQVMsT0FBTyxTQUFQLENBQWlCLENBQWpCLEVBQW9CLE9BQU8sTUFBM0IsQ0FBVDtBQUNIOztBQUVEOzs7O0FBSUEsZ0JBQUksT0FBTyxLQUFLLE1BQUwsQ0FBWSxNQUFaLENBQVAsS0FBK0IsVUFBbkMsRUFBK0M7QUFDM0Msb0JBQUksQ0FBQyxLQUFLLE1BQUwsQ0FBWSxNQUFaLEVBQW9CLEtBQXBCLENBQTBCLElBQTFCLEVBQWdDLENBQUMsS0FBRCxFQUFRLEtBQVIsQ0FBaEMsQ0FBTCxFQUFzRDtBQUNsRCw2QkFBUyxJQUFUO0FBQ0g7QUFDSixhQUpELE1BSU8sSUFBSSxPQUFPLFNBQVAsQ0FBaUIsQ0FBakIsRUFBb0IsQ0FBcEIsTUFBMkIsV0FBL0IsRUFBNEM7QUFDL0M7QUFDQSx5QkFBUyxPQUFPLFNBQVAsQ0FBaUIsQ0FBakIsRUFBb0IsT0FBTyxNQUEzQixDQUFUOztBQUVBLG9CQUFJLE9BQU8sS0FBSyxRQUFMLENBQWMsTUFBZCxDQUFQLEtBQWlDLFVBQXJDLEVBQWlEO0FBQzdDLHdCQUFJLEtBQUssUUFBTCxDQUFjLE1BQWQsRUFBc0IsS0FBdEIsQ0FBNEIsSUFBNUIsRUFBa0MsQ0FBQyxNQUFNLEtBQVAsRUFBYyxLQUFkLEVBQXFCLEtBQXJCLENBQWxDLE1BQW1FLEtBQXZFLEVBQThFO0FBQzFFLGlDQUFTLElBQVQ7QUFDSDtBQUNKO0FBQ0o7O0FBRUQ7Ozs7QUFJQSxnQkFBSSxNQUFKLEVBQVk7QUFDUjtBQUNBLG9CQUFJLFNBQVMsS0FBSyxRQUFMLENBQWMsTUFBTSxJQUFOLEdBQWEsR0FBYixHQUFtQixNQUFqQyxLQUE0QyxLQUFLLFFBQUwsQ0FBYyxNQUFkLENBQTVDLElBQXFFLFNBQVMsUUFBVCxDQUFrQixNQUFsQixDQUFsRjtBQUFBLG9CQUNJLFVBQVUsb0NBQW9DLE1BQU0sT0FBMUMsR0FBb0QsU0FEbEU7O0FBR0Esb0JBQUksTUFBSixFQUFZO0FBQ1IsOEJBQVUsT0FBTyxPQUFQLENBQWUsSUFBZixFQUFxQixNQUFNLE9BQTNCLENBQVY7O0FBRUEsd0JBQUksS0FBSixFQUFXO0FBQ1Asa0NBQVUsUUFBUSxPQUFSLENBQWdCLElBQWhCLEVBQXVCLEtBQUssTUFBTCxDQUFZLEtBQVosQ0FBRCxHQUF1QixLQUFLLE1BQUwsQ0FBWSxLQUFaLEVBQW1CLE9BQTFDLEdBQW9ELEtBQTFFLENBQVY7QUFDSDtBQUNKOztBQUVELG9CQUFJLGFBQUo7QUFDQSxxQkFBSyxJQUFJLENBQVQsRUFBWSxJQUFJLEtBQUssTUFBTCxDQUFZLE1BQTVCLEVBQW9DLEtBQUssQ0FBekMsRUFBNEM7QUFDeEMsd0JBQUksTUFBTSxFQUFOLEtBQWEsS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLEVBQWhDLEVBQW9DO0FBQ2hDLHdDQUFnQixLQUFLLE1BQUwsQ0FBWSxDQUFaLENBQWhCO0FBQ0g7QUFDSjs7QUFFRCxvQkFBSSxjQUFjLGlCQUFpQjtBQUMvQix3QkFBSSxNQUFNLEVBRHFCO0FBRS9CLDZCQUFTLE1BQU0sT0FGZ0I7QUFHL0IsNkJBQVMsTUFBTSxPQUhnQjtBQUkvQiwwQkFBTSxNQUFNLElBSm1CO0FBSy9CLDZCQUFTLE9BTHNCO0FBTS9CLDhCQUFVLEVBTnFCO0FBTy9CLDBCQUFNO0FBUHlCLGlCQUFuQztBQVNBLDRCQUFZLFFBQVosQ0FBcUIsSUFBckIsQ0FBMEIsT0FBMUI7QUFDQSxvQkFBSSxDQUFDLGFBQUwsRUFBb0IsS0FBSyxNQUFMLENBQVksSUFBWixDQUFpQixXQUFqQjtBQUN2QjtBQUNKO0FBQ0osS0E5RkQ7O0FBZ0dBOzs7OztBQUtBLGtCQUFjLFNBQWQsQ0FBd0IsYUFBeEIsR0FBd0MsVUFBUyxJQUFULEVBQWU7QUFDbkQsWUFBSSxDQUFDLEtBQUssS0FBTCxDQUFXLE9BQVgsQ0FBRCxJQUF3QixDQUFDLEtBQUssS0FBTCxDQUFXLFNBQVgsQ0FBN0IsRUFBb0Q7QUFDaEQsbUJBQU8sS0FBUDtBQUNIOztBQUVELFlBQUksWUFBWSxJQUFJLElBQUosRUFBaEI7QUFBQSxZQUNJLGNBREo7O0FBR0EsWUFBSSxDQUFDLEtBQUssS0FBTCxDQUFXLE9BQVgsQ0FBTCxFQUEwQjtBQUN0Qiw2QkFBaUIsS0FBSyxLQUFMLENBQVcsR0FBWCxDQUFqQjtBQUNBLHNCQUFVLFdBQVYsQ0FBc0IsZUFBZSxDQUFmLENBQXRCO0FBQ0Esc0JBQVUsUUFBVixDQUFtQixlQUFlLENBQWYsSUFBb0IsQ0FBdkM7QUFDQSxzQkFBVSxPQUFWLENBQWtCLGVBQWUsQ0FBZixDQUFsQjtBQUNIO0FBQ0QsZUFBTyxTQUFQO0FBQ0gsS0FmRDs7QUFpQkE7Ozs7O0FBS0Esa0JBQWMsU0FBZCxDQUF3QixNQUF4QixHQUFpQztBQUM3QjtBQUFVLDhCQUFTLEtBQVQsRUFBZ0I7QUFDdEIsb0JBQUksUUFBUSxNQUFNLEtBQWxCOztBQUVBLG9CQUFLLE1BQU0sSUFBTixLQUFlLFVBQWhCLElBQWdDLE1BQU0sSUFBTixLQUFlLE9BQW5ELEVBQTZEO0FBQ3pELDJCQUFRLE1BQU0sT0FBTixLQUFrQixJQUExQjtBQUNIOztBQUVELHVCQUFRLFVBQVUsSUFBVixJQUFrQixVQUFVLEVBQXBDO0FBQ0g7O0FBUkQ7QUFBQSxXQUQ2Qjs7QUFXN0I7QUFBVyw4QkFBUyxLQUFULEVBQWdCLFdBQWhCLEVBQTRCO0FBQ25DLHVCQUFPLE1BQU0sS0FBTixLQUFnQixXQUF2QjtBQUNIOztBQUZEO0FBQUEsV0FYNkI7O0FBZTdCO0FBQVMsNkJBQVMsS0FBVCxFQUFnQixTQUFoQixFQUEyQjtBQUNoQyxvQkFBSSxLQUFLLEtBQUssSUFBTCxDQUFVLFNBQVYsQ0FBVDs7QUFFQSxvQkFBSSxFQUFKLEVBQVE7QUFDSiwyQkFBTyxNQUFNLEtBQU4sS0FBZ0IsR0FBRyxLQUExQjtBQUNIOztBQUVELHVCQUFPLEtBQVA7QUFDSDs7QUFSRDtBQUFBLFdBZjZCOztBQXlCN0I7QUFBYSxpQ0FBUyxLQUFULEVBQWdCO0FBQ3pCLHVCQUFPLFdBQVcsSUFBWCxDQUFnQixNQUFNLEtBQXRCLENBQVA7QUFDSDs7QUFGRDtBQUFBLFdBekI2Qjs7QUE2QjdCO0FBQWMsa0NBQVMsS0FBVCxFQUFnQjtBQUMxQixvQkFBSSxTQUFTLE1BQU0sS0FBTixDQUFZLEtBQVosQ0FBa0IsVUFBbEIsQ0FBYjs7QUFFQSxxQkFBSyxJQUFJLElBQUksQ0FBUixFQUFXLGVBQWUsT0FBTyxNQUF0QyxFQUE4QyxJQUFJLFlBQWxELEVBQWdFLEdBQWhFLEVBQXFFO0FBQ2pFLHdCQUFJLENBQUMsV0FBVyxJQUFYLENBQWdCLE9BQU8sQ0FBUCxDQUFoQixDQUFMLEVBQWlDO0FBQzdCLCtCQUFPLEtBQVA7QUFDSDtBQUNKOztBQUVELHVCQUFPLElBQVA7QUFDSDs7QUFWRDtBQUFBLFdBN0I2Qjs7QUF5QzdCO0FBQVksZ0NBQVMsS0FBVCxFQUFnQixNQUFoQixFQUF3QjtBQUNoQyxvQkFBSSxDQUFDLGFBQWEsSUFBYixDQUFrQixNQUFsQixDQUFMLEVBQWdDO0FBQzVCLDJCQUFPLEtBQVA7QUFDSDs7QUFFRCx1QkFBUSxNQUFNLEtBQU4sQ0FBWSxNQUFaLElBQXNCLFNBQVMsTUFBVCxFQUFpQixFQUFqQixDQUE5QjtBQUNIOztBQU5EO0FBQUEsV0F6QzZCOztBQWlEN0I7QUFBWSxnQ0FBUyxLQUFULEVBQWdCLE1BQWhCLEVBQXdCO0FBQ2hDLG9CQUFJLENBQUMsYUFBYSxJQUFiLENBQWtCLE1BQWxCLENBQUwsRUFBZ0M7QUFDNUIsMkJBQU8sS0FBUDtBQUNIOztBQUVELHVCQUFRLE1BQU0sS0FBTixDQUFZLE1BQVosSUFBc0IsU0FBUyxNQUFULEVBQWlCLEVBQWpCLENBQTlCO0FBQ0g7O0FBTkQ7QUFBQSxXQWpENkI7O0FBeUQ3QjtBQUFjLGtDQUFTLEtBQVQsRUFBZ0IsTUFBaEIsRUFBd0I7QUFDbEMsb0JBQUksQ0FBQyxhQUFhLElBQWIsQ0FBa0IsTUFBbEIsQ0FBTCxFQUFnQztBQUM1QiwyQkFBTyxLQUFQO0FBQ0g7O0FBRUQsdUJBQVEsTUFBTSxLQUFOLENBQVksTUFBWixLQUF1QixTQUFTLE1BQVQsRUFBaUIsRUFBakIsQ0FBL0I7QUFDSDs7QUFORDtBQUFBLFdBekQ2Qjs7QUFpRTdCO0FBQWMsa0NBQVMsS0FBVCxFQUFnQixLQUFoQixFQUF1QjtBQUNqQyxvQkFBSSxDQUFDLGFBQWEsSUFBYixDQUFrQixNQUFNLEtBQXhCLENBQUwsRUFBcUM7QUFDakMsMkJBQU8sS0FBUDtBQUNIOztBQUVELHVCQUFRLFdBQVcsTUFBTSxLQUFqQixJQUEwQixXQUFXLEtBQVgsQ0FBbEM7QUFDSDs7QUFORDtBQUFBLFdBakU2Qjs7QUF5RTdCO0FBQVcsK0JBQVMsS0FBVCxFQUFnQixLQUFoQixFQUF1QjtBQUM5QixvQkFBSSxDQUFDLGFBQWEsSUFBYixDQUFrQixNQUFNLEtBQXhCLENBQUwsRUFBcUM7QUFDakMsMkJBQU8sS0FBUDtBQUNIOztBQUVELHVCQUFRLFdBQVcsTUFBTSxLQUFqQixJQUEwQixXQUFXLEtBQVgsQ0FBbEM7QUFDSDs7QUFORDtBQUFBLFdBekU2Qjs7QUFpRjdCO0FBQU8sMkJBQVMsS0FBVCxFQUFnQjtBQUNuQix1QkFBUSxXQUFXLElBQVgsQ0FBZ0IsTUFBTSxLQUF0QixDQUFSO0FBQ0g7O0FBRkQ7QUFBQSxXQWpGNkI7O0FBcUY3QjtBQUFlLG1DQUFTLEtBQVQsRUFBZ0I7QUFDM0IsdUJBQVEsa0JBQWtCLElBQWxCLENBQXVCLE1BQU0sS0FBN0IsQ0FBUjtBQUNIOztBQUZEO0FBQUEsV0FyRjZCOztBQXlGN0I7QUFBWSxnQ0FBUyxLQUFULEVBQWdCO0FBQ3hCLHVCQUFRLGVBQWUsSUFBZixDQUFvQixNQUFNLEtBQTFCLENBQVI7QUFDSDs7QUFGRDtBQUFBLFdBekY2Qjs7QUE2RjdCO0FBQVMsNkJBQVMsS0FBVCxFQUFnQjtBQUNyQix1QkFBUSxhQUFhLElBQWIsQ0FBa0IsTUFBTSxLQUF4QixDQUFSO0FBQ0g7O0FBRkQ7QUFBQSxXQTdGNkI7O0FBaUc3QjtBQUFTLDZCQUFTLEtBQVQsRUFBZ0I7QUFDckIsdUJBQVEsYUFBYSxJQUFiLENBQWtCLE1BQU0sS0FBeEIsQ0FBUjtBQUNIOztBQUZEO0FBQUEsV0FqRzZCOztBQXFHN0I7QUFBUyw2QkFBUyxLQUFULEVBQWdCO0FBQ3JCLHVCQUFRLGFBQWEsSUFBYixDQUFrQixNQUFNLEtBQXhCLENBQVI7QUFDSDs7QUFGRDtBQUFBLFdBckc2Qjs7QUF5RzdCO0FBQVksZ0NBQVMsS0FBVCxFQUFnQjtBQUN4Qix1QkFBUSxhQUFhLElBQWIsQ0FBa0IsTUFBTSxLQUF4QixDQUFSO0FBQ0g7O0FBRkQ7QUFBQSxXQXpHNkI7O0FBNkc3QjtBQUFvQix3Q0FBUyxLQUFULEVBQWdCO0FBQ2hDLHVCQUFRLG1CQUFtQixJQUFuQixDQUF3QixNQUFNLEtBQTlCLENBQVI7QUFDSDs7QUFGRDtBQUFBLFdBN0c2Qjs7QUFpSDdCO0FBQVUsOEJBQVMsS0FBVCxFQUFnQjtBQUN0Qix1QkFBUSxRQUFRLElBQVIsQ0FBYSxNQUFNLEtBQW5CLENBQVI7QUFDSDs7QUFGRDtBQUFBLFdBakg2Qjs7QUFxSDdCO0FBQWMsa0NBQVMsS0FBVCxFQUFnQjtBQUMxQix1QkFBUSxZQUFZLElBQVosQ0FBaUIsTUFBTSxLQUF2QixDQUFSO0FBQ0g7O0FBRkQ7QUFBQSxXQXJINkI7O0FBeUg3QjtBQUFXLCtCQUFTLEtBQVQsRUFBZ0I7QUFDdkIsdUJBQVEsU0FBUyxJQUFULENBQWMsTUFBTSxLQUFwQixDQUFSO0FBQ0g7O0FBRkQ7QUFBQSxXQXpINkI7O0FBNkg3QjtBQUFtQix1Q0FBUyxLQUFULEVBQWU7QUFDOUI7QUFDQTtBQUNBLG9CQUFJLENBQUMsaUJBQWlCLElBQWpCLENBQXNCLE1BQU0sS0FBNUIsQ0FBTCxFQUF5QyxPQUFPLEtBQVA7O0FBRXpDO0FBQ0Esb0JBQUksU0FBUyxDQUFiO0FBQUEsb0JBQWdCLFNBQVMsQ0FBekI7QUFBQSxvQkFBNEIsUUFBUSxLQUFwQztBQUNBLG9CQUFJLGdCQUFnQixNQUFNLEtBQU4sQ0FBWSxPQUFaLENBQW9CLEtBQXBCLEVBQTJCLEVBQTNCLENBQXBCOztBQUVBLHFCQUFLLElBQUksSUFBSSxjQUFjLE1BQWQsR0FBdUIsQ0FBcEMsRUFBdUMsS0FBSyxDQUE1QyxFQUErQyxHQUEvQyxFQUFvRDtBQUNoRCx3QkFBSSxTQUFTLGNBQWMsTUFBZCxDQUFxQixDQUFyQixDQUFiO0FBQ0EsNkJBQVMsU0FBUyxNQUFULEVBQWlCLEVBQWpCLENBQVQ7QUFDQSx3QkFBSSxLQUFKLEVBQVc7QUFDUCw0QkFBSSxDQUFDLFVBQVUsQ0FBWCxJQUFnQixDQUFwQixFQUF1QixVQUFVLENBQVY7QUFDMUI7O0FBRUQsOEJBQVUsTUFBVjtBQUNBLDRCQUFRLENBQUMsS0FBVDtBQUNIOztBQUVELHVCQUFRLFNBQVMsRUFBVixLQUFrQixDQUF6QjtBQUNIOztBQXJCRDtBQUFBLFdBN0g2Qjs7QUFvSjdCO0FBQWMsa0NBQVMsS0FBVCxFQUFlLElBQWYsRUFBcUI7QUFDL0Isb0JBQUksTUFBTSxJQUFOLEtBQWUsTUFBbkIsRUFBMkI7QUFDdkIsMkJBQU8sSUFBUDtBQUNIOztBQUVELG9CQUFJLE1BQU0sTUFBTSxLQUFOLENBQVksTUFBWixDQUFvQixNQUFNLEtBQU4sQ0FBWSxXQUFaLENBQXdCLEdBQXhCLElBQStCLENBQW5ELENBQVY7QUFBQSxvQkFDSSxZQUFZLEtBQUssS0FBTCxDQUFXLEdBQVgsQ0FEaEI7QUFBQSxvQkFFSSxVQUFVLEtBRmQ7QUFBQSxvQkFHSSxJQUFJLENBSFI7QUFBQSxvQkFJSSxNQUFNLFVBQVUsTUFKcEI7O0FBTUEscUJBQUssQ0FBTCxFQUFRLElBQUksR0FBWixFQUFpQixHQUFqQixFQUFzQjtBQUNsQix3QkFBSSxJQUFJLFdBQUosTUFBcUIsVUFBVSxDQUFWLEVBQWEsV0FBYixFQUF6QixFQUFxRCxVQUFVLElBQVY7QUFDeEQ7O0FBRUQsdUJBQU8sT0FBUDtBQUNIOztBQWhCRDtBQUFBLFdBcEo2Qjs7QUFzSzdCO0FBQW1CLHVDQUFVLEtBQVYsRUFBaUIsSUFBakIsRUFBdUI7QUFDdEMsb0JBQUksY0FBYyxLQUFLLGFBQUwsQ0FBbUIsTUFBTSxLQUF6QixDQUFsQjtBQUFBLG9CQUNJLFlBQVksS0FBSyxhQUFMLENBQW1CLElBQW5CLENBRGhCOztBQUdBLG9CQUFJLENBQUMsU0FBRCxJQUFjLENBQUMsV0FBbkIsRUFBZ0M7QUFDNUIsMkJBQU8sS0FBUDtBQUNIOztBQUVELHVCQUFPLGNBQWMsU0FBckI7QUFDSDs7QUFURDtBQUFBLFdBdEs2Qjs7QUFpTDdCO0FBQWdCLG9DQUFVLEtBQVYsRUFBaUIsSUFBakIsRUFBdUI7QUFDbkMsb0JBQUksY0FBYyxLQUFLLGFBQUwsQ0FBbUIsTUFBTSxLQUF6QixDQUFsQjtBQUFBLG9CQUNJLFlBQVksS0FBSyxhQUFMLENBQW1CLElBQW5CLENBRGhCOztBQUdBLG9CQUFJLENBQUMsU0FBRCxJQUFjLENBQUMsV0FBbkIsRUFBZ0M7QUFDNUIsMkJBQU8sS0FBUDtBQUNIOztBQUVELHVCQUFPLGNBQWMsU0FBckI7QUFDSDs7QUFURDtBQUFBLFdBakw2Qjs7QUE0TDdCO0FBQTRCLGdEQUFVLEtBQVYsRUFBaUIsSUFBakIsRUFBdUI7QUFDL0Msb0JBQUksY0FBYyxLQUFLLGFBQUwsQ0FBbUIsTUFBTSxLQUF6QixDQUFsQjtBQUFBLG9CQUNJLFlBQVksS0FBSyxhQUFMLENBQW1CLElBQW5CLENBRGhCOztBQUdBLG9CQUFJLENBQUMsU0FBRCxJQUFjLENBQUMsV0FBbkIsRUFBZ0M7QUFDNUIsMkJBQU8sS0FBUDtBQUNIOztBQUVELHVCQUFPLGVBQWUsU0FBdEI7QUFDSDs7QUFURDtBQUFBLFdBNUw2Qjs7QUF1TTdCO0FBQXlCLDZDQUFVLEtBQVYsRUFBaUIsSUFBakIsRUFBdUI7QUFDNUMsb0JBQUksY0FBYyxLQUFLLGFBQUwsQ0FBbUIsTUFBTSxLQUF6QixDQUFsQjtBQUFBLG9CQUNJLFlBQVksS0FBSyxhQUFMLENBQW1CLElBQW5CLENBRGhCOztBQUdBLG9CQUFJLENBQUMsU0FBRCxJQUFjLENBQUMsV0FBbkIsRUFBZ0M7QUFDNUIsMkJBQU8sS0FBUDtBQUNIOztBQUVELHVCQUFPLGVBQWUsU0FBdEI7QUFDSDs7QUFURDtBQUFBO0FBdk02QixLQUFqQzs7QUFtTkEsV0FBTyxhQUFQLEdBQXVCLGFBQXZCO0FBQ0gsQ0E1bEJELEVBNGxCRyxNQTVsQkgsRUE0bEJXLFFBNWxCWDs7QUE4bEJBOzs7QUFHQSxJQUFJLE9BQU8sTUFBUCxLQUFrQixXQUFsQixJQUFpQyxPQUFPLE9BQTVDLEVBQXFEO0FBQ2pELFdBQU8sT0FBUCxHQUFpQixhQUFqQjtBQUNIIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oKXtmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc31yZXR1cm4gZX0pKCkiLCJ2YXIgcHJldiwgbmV4dCwgc2xpZGVzLCBwb3NpdGlvbjtcclxuXHJcbmZ1bmN0aW9uIFByZXZpb3VzKCkge1xyXG4gIGlmIChwb3NpdGlvbiA+IDEpIHtcclxuICAgIHBvc2l0aW9uIC09IDE7XHJcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHNsaWRlcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICBzbGlkZXNbaV0uc3R5bGUudHJhbnNmb3JtID0gJ3RyYW5zbGF0ZVgoLScgKyAoKHBvc2l0aW9uIC0gMSkgKiAyMSkgKyAndncpJztcclxuICAgIH1cclxuICB9IGVsc2UgaWYgKHBvc2l0aW9uID09IDEpIHtcclxuICAgIGZvciAodmFyIGogPSAwOyBqIDwgc2xpZGVzLmxlbmd0aDsgaisrKSB7XHJcbiAgICAgIHNsaWRlc1tqXS5zdHlsZS50cmFuc2Zvcm0gPSAndHJhbnNsYXRlWCgwdncpJztcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIE5leHQoKSB7XHJcbiAgaWYgKHBvc2l0aW9uIDwgKHNsaWRlcy5sZW5ndGggLSAyKSkge1xyXG4gICAgcG9zaXRpb24gKz0gMTtcclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc2xpZGVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIHNsaWRlc1tpXS5zdHlsZS50cmFuc2Zvcm0gPSAndHJhbnNsYXRlWCgtJyArICgocG9zaXRpb24gLSAxKSAqIDIxKSArICd2dyknO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxud2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBmdW5jdGlvbiAoKSB7XHJcbiAgcHJldiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNwcmV2Jyk7XHJcbiAgbmV4dCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNuZXh0Jyk7XHJcbiAgc2xpZGVzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnI3NsaWRlciA+IC5zbGlkZScpO1xyXG4gIHBvc2l0aW9uID0gMTtcclxuXHJcbiAgcHJldi5vbmNsaWNrID0gUHJldmlvdXM7XHJcbiAgbmV4dC5vbmNsaWNrID0gTmV4dDtcclxufSk7XHJcbiIsImltcG9ydCAnLi92YWxpZGF0ZSc7XHJcbmltcG9ydCAnLi9jYXJvdXNlbCc7IiwiLypcbiAqIHZhbGlkYXRlLmpzIDIuMC4xXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTEgLSAyMDE1IFJpY2sgSGFycmlzb24sIGh0dHA6Ly9yaWNraGFycmlzb24ubWVcbiAqIHZhbGlkYXRlLmpzIGlzIG9wZW4gc291cmNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UuXG4gKiBQb3J0aW9ucyBvZiB2YWxpZGF0ZS5qcyBhcmUgaW5zcGlyZWQgYnkgQ29kZUlnbml0ZXIuXG4gKiBodHRwOi8vcmlja2hhcnJpc29uLmdpdGh1Yi5jb20vdmFsaWRhdGUuanNcbiAqL1xuXG4oZnVuY3Rpb24od2luZG93LCBkb2N1bWVudCwgdW5kZWZpbmVkKSB7XG4gICAgLypcbiAgICAgKiBJZiB5b3Ugd291bGQgbGlrZSBhbiBhcHBsaWNhdGlvbi13aWRlIGNvbmZpZywgY2hhbmdlIHRoZXNlIGRlZmF1bHRzLlxuICAgICAqIE90aGVyd2lzZSwgdXNlIHRoZSBzZXRNZXNzYWdlKCkgZnVuY3Rpb24gdG8gY29uZmlndXJlIGZvcm0gc3BlY2lmaWMgbWVzc2FnZXMuXG4gICAgICovXG5cbiAgICB2YXIgZGVmYXVsdHMgPSB7XG4gICAgICAgIG1lc3NhZ2VzOiB7XG4gICAgICAgICAgICByZXF1aXJlZDogJ1RoZSAlcyBmaWVsZCBpcyByZXF1aXJlZC4nLFxuICAgICAgICAgICAgbWF0Y2hlczogJ1RoZSAlcyBmaWVsZCBkb2VzIG5vdCBtYXRjaCB0aGUgJXMgZmllbGQuJyxcbiAgICAgICAgICAgIFwiZGVmYXVsdFwiOiAnVGhlICVzIGZpZWxkIGlzIHN0aWxsIHNldCB0byBkZWZhdWx0LCBwbGVhc2UgY2hhbmdlLicsXG4gICAgICAgICAgICB2YWxpZF9lbWFpbDogJ1RoZSAlcyBmaWVsZCBtdXN0IGNvbnRhaW4gYSB2YWxpZCBlbWFpbCBhZGRyZXNzLicsXG4gICAgICAgICAgICB2YWxpZF9lbWFpbHM6ICdUaGUgJXMgZmllbGQgbXVzdCBjb250YWluIGFsbCB2YWxpZCBlbWFpbCBhZGRyZXNzZXMuJyxcbiAgICAgICAgICAgIG1pbl9sZW5ndGg6ICdUaGUgJXMgZmllbGQgbXVzdCBiZSBhdCBsZWFzdCAlcyBjaGFyYWN0ZXJzIGluIGxlbmd0aC4nLFxuICAgICAgICAgICAgbWF4X2xlbmd0aDogJ1RoZSAlcyBmaWVsZCBtdXN0IG5vdCBleGNlZWQgJXMgY2hhcmFjdGVycyBpbiBsZW5ndGguJyxcbiAgICAgICAgICAgIGV4YWN0X2xlbmd0aDogJ1RoZSAlcyBmaWVsZCBtdXN0IGJlIGV4YWN0bHkgJXMgY2hhcmFjdGVycyBpbiBsZW5ndGguJyxcbiAgICAgICAgICAgIGdyZWF0ZXJfdGhhbjogJ1RoZSAlcyBmaWVsZCBtdXN0IGNvbnRhaW4gYSBudW1iZXIgZ3JlYXRlciB0aGFuICVzLicsXG4gICAgICAgICAgICBsZXNzX3RoYW46ICdUaGUgJXMgZmllbGQgbXVzdCBjb250YWluIGEgbnVtYmVyIGxlc3MgdGhhbiAlcy4nLFxuICAgICAgICAgICAgYWxwaGE6ICdUaGUgJXMgZmllbGQgbXVzdCBvbmx5IGNvbnRhaW4gYWxwaGFiZXRpY2FsIGNoYXJhY3RlcnMuJyxcbiAgICAgICAgICAgIGFscGhhX251bWVyaWM6ICdUaGUgJXMgZmllbGQgbXVzdCBvbmx5IGNvbnRhaW4gYWxwaGEtbnVtZXJpYyBjaGFyYWN0ZXJzLicsXG4gICAgICAgICAgICBhbHBoYV9kYXNoOiAnVGhlICVzIGZpZWxkIG11c3Qgb25seSBjb250YWluIGFscGhhLW51bWVyaWMgY2hhcmFjdGVycywgdW5kZXJzY29yZXMsIGFuZCBkYXNoZXMuJyxcbiAgICAgICAgICAgIG51bWVyaWM6ICdUaGUgJXMgZmllbGQgbXVzdCBjb250YWluIG9ubHkgbnVtYmVycy4nLFxuICAgICAgICAgICAgaW50ZWdlcjogJ1RoZSAlcyBmaWVsZCBtdXN0IGNvbnRhaW4gYW4gaW50ZWdlci4nLFxuICAgICAgICAgICAgZGVjaW1hbDogJ1RoZSAlcyBmaWVsZCBtdXN0IGNvbnRhaW4gYSBkZWNpbWFsIG51bWJlci4nLFxuICAgICAgICAgICAgaXNfbmF0dXJhbDogJ1RoZSAlcyBmaWVsZCBtdXN0IGNvbnRhaW4gb25seSBwb3NpdGl2ZSBudW1iZXJzLicsXG4gICAgICAgICAgICBpc19uYXR1cmFsX25vX3plcm86ICdUaGUgJXMgZmllbGQgbXVzdCBjb250YWluIGEgbnVtYmVyIGdyZWF0ZXIgdGhhbiB6ZXJvLicsXG4gICAgICAgICAgICB2YWxpZF9pcDogJ1RoZSAlcyBmaWVsZCBtdXN0IGNvbnRhaW4gYSB2YWxpZCBJUC4nLFxuICAgICAgICAgICAgdmFsaWRfYmFzZTY0OiAnVGhlICVzIGZpZWxkIG11c3QgY29udGFpbiBhIGJhc2U2NCBzdHJpbmcuJyxcbiAgICAgICAgICAgIHZhbGlkX2NyZWRpdF9jYXJkOiAnVGhlICVzIGZpZWxkIG11c3QgY29udGFpbiBhIHZhbGlkIGNyZWRpdCBjYXJkIG51bWJlci4nLFxuICAgICAgICAgICAgaXNfZmlsZV90eXBlOiAnVGhlICVzIGZpZWxkIG11c3QgY29udGFpbiBvbmx5ICVzIGZpbGVzLicsXG4gICAgICAgICAgICB2YWxpZF91cmw6ICdUaGUgJXMgZmllbGQgbXVzdCBjb250YWluIGEgdmFsaWQgVVJMLicsXG4gICAgICAgICAgICBncmVhdGVyX3RoYW5fZGF0ZTogJ1RoZSAlcyBmaWVsZCBtdXN0IGNvbnRhaW4gYSBtb3JlIHJlY2VudCBkYXRlIHRoYW4gJXMuJyxcbiAgICAgICAgICAgIGxlc3NfdGhhbl9kYXRlOiAnVGhlICVzIGZpZWxkIG11c3QgY29udGFpbiBhbiBvbGRlciBkYXRlIHRoYW4gJXMuJyxcbiAgICAgICAgICAgIGdyZWF0ZXJfdGhhbl9vcl9lcXVhbF9kYXRlOiAnVGhlICVzIGZpZWxkIG11c3QgY29udGFpbiBhIGRhdGUgdGhhdFxcJ3MgYXQgbGVhc3QgYXMgcmVjZW50IGFzICVzLicsXG4gICAgICAgICAgICBsZXNzX3RoYW5fb3JfZXF1YWxfZGF0ZTogJ1RoZSAlcyBmaWVsZCBtdXN0IGNvbnRhaW4gYSBkYXRlIHRoYXRcXCdzICVzIG9yIG9sZGVyLidcbiAgICAgICAgfSxcbiAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uKGVycm9ycykge1xuXG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgLypcbiAgICAgKiBEZWZpbmUgdGhlIHJlZ3VsYXIgZXhwcmVzc2lvbnMgdGhhdCB3aWxsIGJlIHVzZWRcbiAgICAgKi9cblxuICAgIHZhciBydWxlUmVnZXggPSAvXiguKz8pXFxbKC4rKVxcXSQvLFxuICAgICAgICBudW1lcmljUmVnZXggPSAvXlswLTldKyQvLFxuICAgICAgICBpbnRlZ2VyUmVnZXggPSAvXlxcLT9bMC05XSskLyxcbiAgICAgICAgZGVjaW1hbFJlZ2V4ID0gL15cXC0/WzAtOV0qXFwuP1swLTldKyQvLFxuICAgICAgICBlbWFpbFJlZ2V4ID0gL15bYS16QS1aMC05LiEjJCUmJyorLz0/Xl9ge3x9fi1dK0BbYS16QS1aMC05XSg/OlthLXpBLVowLTktXXswLDYxfVthLXpBLVowLTldKT8oPzpcXC5bYS16QS1aMC05XSg/OlthLXpBLVowLTktXXswLDYxfVthLXpBLVowLTldKT8pKiQvLFxuICAgICAgICBhbHBoYVJlZ2V4ID0gL15bYS16XSskL2ksXG4gICAgICAgIGFscGhhTnVtZXJpY1JlZ2V4ID0gL15bYS16MC05XSskL2ksXG4gICAgICAgIGFscGhhRGFzaFJlZ2V4ID0gL15bYS16MC05X1xcLV0rJC9pLFxuICAgICAgICBuYXR1cmFsUmVnZXggPSAvXlswLTldKyQvaSxcbiAgICAgICAgbmF0dXJhbE5vWmVyb1JlZ2V4ID0gL15bMS05XVswLTldKiQvaSxcbiAgICAgICAgaXBSZWdleCA9IC9eKCgyNVswLTVdfDJbMC00XVswLTldfDFbMC05XXsyfXxbMC05XXsxLDJ9KVxcLil7M30oMjVbMC01XXwyWzAtNF1bMC05XXwxWzAtOV17Mn18WzAtOV17MSwyfSkkL2ksXG4gICAgICAgIGJhc2U2NFJlZ2V4ID0gL1teYS16QS1aMC05XFwvXFwrPV0vaSxcbiAgICAgICAgbnVtZXJpY0Rhc2hSZWdleCA9IC9eW1xcZFxcLVxcc10rJC8sXG4gICAgICAgIHVybFJlZ2V4ID0gL14oKGh0dHB8aHR0cHMpOlxcL1xcLyhcXHcrOnswLDF9XFx3KkApPyhcXFMrKXwpKDpbMC05XSspPyhcXC98XFwvKFtcXHcjITouPys9JiVAIVxcLVxcL10pKT8kLyxcbiAgICAgICAgZGF0ZVJlZ2V4ID0gL1xcZHs0fS1cXGR7MSwyfS1cXGR7MSwyfS87XG5cbiAgICAvKlxuICAgICAqIFRoZSBleHBvc2VkIHB1YmxpYyBvYmplY3QgdG8gdmFsaWRhdGUgYSBmb3JtOlxuICAgICAqXG4gICAgICogQHBhcmFtIGZvcm1OYW1lT3JOb2RlIC0gU3RyaW5nIC0gVGhlIG5hbWUgYXR0cmlidXRlIG9mIHRoZSBmb3JtIChpLmUuIDxmb3JtIG5hbWU9XCJteUZvcm1cIj48L2Zvcm0+KSBvciBub2RlIG9mIHRoZSBmb3JtIGVsZW1lbnRcbiAgICAgKiBAcGFyYW0gZmllbGRzIC0gQXJyYXkgLSBbe1xuICAgICAqICAgICBuYW1lOiBUaGUgbmFtZSBvZiB0aGUgZWxlbWVudCAoaS5lLiA8aW5wdXQgbmFtZT1cIm15RmllbGRcIiAvPilcbiAgICAgKiAgICAgZGlzcGxheTogJ0ZpZWxkIE5hbWUnXG4gICAgICogICAgIHJ1bGVzOiByZXF1aXJlZHxtYXRjaGVzW3Bhc3N3b3JkX2NvbmZpcm1dXG4gICAgICogfV1cbiAgICAgKiBAcGFyYW0gY2FsbGJhY2sgLSBGdW5jdGlvbiAtIFRoZSBjYWxsYmFjayBhZnRlciB2YWxpZGF0aW9uIGhhcyBiZWVuIHBlcmZvcm1lZC5cbiAgICAgKiAgICAgQGFyZ3VtZW50IGVycm9ycyAtIEFuIGFycmF5IG9mIHZhbGlkYXRpb24gZXJyb3JzXG4gICAgICogICAgIEBhcmd1bWVudCBldmVudCAtIFRoZSBqYXZhc2NyaXB0IGV2ZW50XG4gICAgICovXG5cbiAgICB2YXIgRm9ybVZhbGlkYXRvciA9IGZ1bmN0aW9uKGZvcm1OYW1lT3JOb2RlLCBmaWVsZHMsIGNhbGxiYWNrKSB7XG4gICAgICAgIHRoaXMuY2FsbGJhY2sgPSBjYWxsYmFjayB8fCBkZWZhdWx0cy5jYWxsYmFjaztcbiAgICAgICAgdGhpcy5lcnJvcnMgPSBbXTtcbiAgICAgICAgdGhpcy5maWVsZHMgPSB7fTtcbiAgICAgICAgdGhpcy5mb3JtID0gdGhpcy5fZm9ybUJ5TmFtZU9yTm9kZShmb3JtTmFtZU9yTm9kZSkgfHwge307XG4gICAgICAgIHRoaXMubWVzc2FnZXMgPSB7fTtcbiAgICAgICAgdGhpcy5oYW5kbGVycyA9IHt9O1xuICAgICAgICB0aGlzLmNvbmRpdGlvbmFscyA9IHt9O1xuXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBmaWVsZExlbmd0aCA9IGZpZWxkcy5sZW5ndGg7IGkgPCBmaWVsZExlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICB2YXIgZmllbGQgPSBmaWVsZHNbaV07XG5cbiAgICAgICAgICAgIC8vIElmIHBhc3NlZCBpbiBpbmNvcnJlY3RseSwgd2UgbmVlZCB0byBza2lwIHRoZSBmaWVsZC5cbiAgICAgICAgICAgIGlmICgoIWZpZWxkLm5hbWUgJiYgIWZpZWxkLm5hbWVzKSB8fCAhZmllbGQucnVsZXMpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oJ3ZhbGlkYXRlLmpzOiBUaGUgZm9sbG93aW5nIGZpZWxkIGlzIGJlaW5nIHNraXBwZWQgZHVlIHRvIGEgbWlzY29uZmlndXJhdGlvbjonKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oZmllbGQpO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybignQ2hlY2sgdG8gZW5zdXJlIHlvdSBoYXZlIHByb3Blcmx5IGNvbmZpZ3VyZWQgYSBuYW1lIGFuZCBydWxlcyBmb3IgdGhpcyBmaWVsZCcpO1xuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvKlxuICAgICAgICAgICAgICogQnVpbGQgdGhlIG1hc3RlciBmaWVsZHMgYXJyYXkgdGhhdCBoYXMgYWxsIHRoZSBpbmZvcm1hdGlvbiBuZWVkZWQgdG8gdmFsaWRhdGVcbiAgICAgICAgICAgICAqL1xuXG4gICAgICAgICAgICBpZiAoZmllbGQubmFtZXMpIHtcbiAgICAgICAgICAgICAgICBmb3IgKHZhciBqID0gMCwgZmllbGROYW1lc0xlbmd0aCA9IGZpZWxkLm5hbWVzLmxlbmd0aDsgaiA8IGZpZWxkTmFtZXNMZW5ndGg7IGorKykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9hZGRGaWVsZChmaWVsZCwgZmllbGQubmFtZXNbal0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5fYWRkRmllbGQoZmllbGQsIGZpZWxkLm5hbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLypcbiAgICAgICAgICogQXR0YWNoIGFuIGV2ZW50IGNhbGxiYWNrIGZvciB0aGUgZm9ybSBzdWJtaXNzaW9uXG4gICAgICAgICAqL1xuXG4gICAgICAgIHZhciBfb25zdWJtaXQgPSB0aGlzLmZvcm0ub25zdWJtaXQ7XG5cbiAgICAgICAgdGhpcy5mb3JtLm9uc3VibWl0ID0gKGZ1bmN0aW9uKHRoYXQpIHtcbiAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhhdC5fdmFsaWRhdGVGb3JtKGV2dCkgJiYgKF9vbnN1Ym1pdCA9PT0gdW5kZWZpbmVkIHx8IF9vbnN1Ym1pdCgpKTtcbiAgICAgICAgICAgICAgICB9IGNhdGNoKGUpIHt9XG4gICAgICAgICAgICB9O1xuICAgICAgICB9KSh0aGlzKTtcbiAgICB9LFxuXG4gICAgYXR0cmlidXRlVmFsdWUgPSBmdW5jdGlvbiAoZWxlbWVudCwgYXR0cmlidXRlTmFtZSkge1xuICAgICAgICB2YXIgaTtcblxuICAgICAgICBpZiAoKGVsZW1lbnQubGVuZ3RoID4gMCkgJiYgKGVsZW1lbnRbMF0udHlwZSA9PT0gJ3JhZGlvJyB8fCBlbGVtZW50WzBdLnR5cGUgPT09ICdjaGVja2JveCcpKSB7XG4gICAgICAgICAgICBmb3IgKGkgPSAwLCBlbGVtZW50TGVuZ3RoID0gZWxlbWVudC5sZW5ndGg7IGkgPCBlbGVtZW50TGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBpZiAoZWxlbWVudFtpXS5jaGVja2VkKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBlbGVtZW50W2ldW2F0dHJpYnV0ZU5hbWVdO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGVsZW1lbnRbYXR0cmlidXRlTmFtZV07XG4gICAgfTtcblxuICAgIC8qXG4gICAgICogQHB1YmxpY1xuICAgICAqIFNldHMgYSBjdXN0b20gbWVzc2FnZSBmb3Igb25lIG9mIHRoZSBydWxlc1xuICAgICAqL1xuXG4gICAgRm9ybVZhbGlkYXRvci5wcm90b3R5cGUuc2V0TWVzc2FnZSA9IGZ1bmN0aW9uKHJ1bGUsIG1lc3NhZ2UpIHtcbiAgICAgICAgdGhpcy5tZXNzYWdlc1tydWxlXSA9IG1lc3NhZ2U7XG5cbiAgICAgICAgLy8gcmV0dXJuIHRoaXMgZm9yIGNoYWluaW5nXG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbiAgICAvKlxuICAgICAqIEBwdWJsaWNcbiAgICAgKiBSZWdpc3RlcnMgYSBjYWxsYmFjayBmb3IgYSBjdXN0b20gcnVsZSAoaS5lLiBjYWxsYmFja191c2VybmFtZV9jaGVjaylcbiAgICAgKi9cblxuICAgIEZvcm1WYWxpZGF0b3IucHJvdG90eXBlLnJlZ2lzdGVyQ2FsbGJhY2sgPSBmdW5jdGlvbihuYW1lLCBoYW5kbGVyKSB7XG4gICAgICAgIGlmIChuYW1lICYmIHR5cGVvZiBuYW1lID09PSAnc3RyaW5nJyAmJiBoYW5kbGVyICYmIHR5cGVvZiBoYW5kbGVyID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICB0aGlzLmhhbmRsZXJzW25hbWVdID0gaGFuZGxlcjtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIHJldHVybiB0aGlzIGZvciBjaGFpbmluZ1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4gICAgLypcbiAgICAgKiBAcHVibGljXG4gICAgICogUmVnaXN0ZXJzIGEgY29uZGl0aW9uYWwgZm9yIGEgY3VzdG9tICdkZXBlbmRzJyBydWxlXG4gICAgICovXG5cbiAgICBGb3JtVmFsaWRhdG9yLnByb3RvdHlwZS5yZWdpc3RlckNvbmRpdGlvbmFsID0gZnVuY3Rpb24obmFtZSwgY29uZGl0aW9uYWwpIHtcbiAgICAgICAgaWYgKG5hbWUgJiYgdHlwZW9mIG5hbWUgPT09ICdzdHJpbmcnICYmIGNvbmRpdGlvbmFsICYmIHR5cGVvZiBjb25kaXRpb25hbCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgdGhpcy5jb25kaXRpb25hbHNbbmFtZV0gPSBjb25kaXRpb25hbDtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIHJldHVybiB0aGlzIGZvciBjaGFpbmluZ1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4gICAgLypcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqIERldGVybWluZXMgaWYgYSBmb3JtIGRvbSBub2RlIHdhcyBwYXNzZWQgaW4gb3IganVzdCBhIHN0cmluZyByZXByZXNlbnRpbmcgdGhlIGZvcm0gbmFtZVxuICAgICAqL1xuXG4gICAgRm9ybVZhbGlkYXRvci5wcm90b3R5cGUuX2Zvcm1CeU5hbWVPck5vZGUgPSBmdW5jdGlvbihmb3JtTmFtZU9yTm9kZSkge1xuICAgICAgICByZXR1cm4gKHR5cGVvZiBmb3JtTmFtZU9yTm9kZSA9PT0gJ29iamVjdCcpID8gZm9ybU5hbWVPck5vZGUgOiBkb2N1bWVudC5mb3Jtc1tmb3JtTmFtZU9yTm9kZV07XG4gICAgfTtcblxuICAgIC8qXG4gICAgICogQHByaXZhdGVcbiAgICAgKiBBZGRzIGEgZmlsZSB0byB0aGUgbWFzdGVyIGZpZWxkcyBhcnJheVxuICAgICAqL1xuXG4gICAgRm9ybVZhbGlkYXRvci5wcm90b3R5cGUuX2FkZEZpZWxkID0gZnVuY3Rpb24oZmllbGQsIG5hbWVWYWx1ZSkgIHtcbiAgICAgICAgdGhpcy5maWVsZHNbbmFtZVZhbHVlXSA9IHtcbiAgICAgICAgICAgIG5hbWU6IG5hbWVWYWx1ZSxcbiAgICAgICAgICAgIGRpc3BsYXk6IGZpZWxkLmRpc3BsYXkgfHwgbmFtZVZhbHVlLFxuICAgICAgICAgICAgcnVsZXM6IGZpZWxkLnJ1bGVzLFxuICAgICAgICAgICAgZGVwZW5kczogZmllbGQuZGVwZW5kcyxcbiAgICAgICAgICAgIGlkOiBudWxsLFxuICAgICAgICAgICAgZWxlbWVudDogbnVsbCxcbiAgICAgICAgICAgIHR5cGU6IG51bGwsXG4gICAgICAgICAgICB2YWx1ZTogbnVsbCxcbiAgICAgICAgICAgIGNoZWNrZWQ6IG51bGxcbiAgICAgICAgfTtcbiAgICB9O1xuXG4gICAgLypcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqIFJ1bnMgdGhlIHZhbGlkYXRpb24gd2hlbiB0aGUgZm9ybSBpcyBzdWJtaXR0ZWQuXG4gICAgICovXG5cbiAgICBGb3JtVmFsaWRhdG9yLnByb3RvdHlwZS5fdmFsaWRhdGVGb3JtID0gZnVuY3Rpb24oZXZ0KSB7XG4gICAgICAgIHRoaXMuZXJyb3JzID0gW107XG5cbiAgICAgICAgZm9yICh2YXIga2V5IGluIHRoaXMuZmllbGRzKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5maWVsZHMuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICAgICAgICAgIHZhciBmaWVsZCA9IHRoaXMuZmllbGRzW2tleV0gfHwge30sXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQgPSB0aGlzLmZvcm1bZmllbGQubmFtZV07XG5cbiAgICAgICAgICAgICAgICBpZiAoZWxlbWVudCAmJiBlbGVtZW50ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgZmllbGQuaWQgPSBhdHRyaWJ1dGVWYWx1ZShlbGVtZW50LCAnaWQnKTtcbiAgICAgICAgICAgICAgICAgICAgZmllbGQuZWxlbWVudCA9IGVsZW1lbnQ7XG4gICAgICAgICAgICAgICAgICAgIGZpZWxkLnR5cGUgPSAoZWxlbWVudC5sZW5ndGggPiAwKSA/IGVsZW1lbnRbMF0udHlwZSA6IGVsZW1lbnQudHlwZTtcbiAgICAgICAgICAgICAgICAgICAgZmllbGQudmFsdWUgPSBhdHRyaWJ1dGVWYWx1ZShlbGVtZW50LCAndmFsdWUnKTtcbiAgICAgICAgICAgICAgICAgICAgZmllbGQuY2hlY2tlZCA9IGF0dHJpYnV0ZVZhbHVlKGVsZW1lbnQsICdjaGVja2VkJyk7XG5cbiAgICAgICAgICAgICAgICAgICAgLypcbiAgICAgICAgICAgICAgICAgICAgICogUnVuIHRocm91Z2ggdGhlIHJ1bGVzIGZvciBlYWNoIGZpZWxkLlxuICAgICAgICAgICAgICAgICAgICAgKiBJZiB0aGUgZmllbGQgaGFzIGEgZGVwZW5kcyBjb25kaXRpb25hbCwgb25seSB2YWxpZGF0ZSB0aGUgZmllbGRcbiAgICAgICAgICAgICAgICAgICAgICogaWYgaXQgcGFzc2VzIHRoZSBjdXN0b20gZnVuY3Rpb25cbiAgICAgICAgICAgICAgICAgICAgICovXG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKGZpZWxkLmRlcGVuZHMgJiYgdHlwZW9mIGZpZWxkLmRlcGVuZHMgPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGZpZWxkLmRlcGVuZHMuY2FsbCh0aGlzLCBmaWVsZCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl92YWxpZGF0ZUZpZWxkKGZpZWxkKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChmaWVsZC5kZXBlbmRzICYmIHR5cGVvZiBmaWVsZC5kZXBlbmRzID09PSBcInN0cmluZ1wiICYmIHRoaXMuY29uZGl0aW9uYWxzW2ZpZWxkLmRlcGVuZHNdKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5jb25kaXRpb25hbHNbZmllbGQuZGVwZW5kc10uY2FsbCh0aGlzLGZpZWxkKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX3ZhbGlkYXRlRmllbGQoZmllbGQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fdmFsaWRhdGVGaWVsZChmaWVsZCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodHlwZW9mIHRoaXMuY2FsbGJhY2sgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIHRoaXMuY2FsbGJhY2sodGhpcy5lcnJvcnMsIGV2dCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5lcnJvcnMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgaWYgKGV2dCAmJiBldnQucHJldmVudERlZmF1bHQpIHtcbiAgICAgICAgICAgICAgICBldnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAvLyBJRSB1c2VzIHRoZSBnbG9iYWwgZXZlbnQgdmFyaWFibGVcbiAgICAgICAgICAgICAgICBldmVudC5yZXR1cm5WYWx1ZSA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfTtcblxuICAgIC8qXG4gICAgICogQHByaXZhdGVcbiAgICAgKiBMb29rcyBhdCB0aGUgZmllbGRzIHZhbHVlIGFuZCBldmFsdWF0ZXMgaXQgYWdhaW5zdCB0aGUgZ2l2ZW4gcnVsZXNcbiAgICAgKi9cblxuICAgIEZvcm1WYWxpZGF0b3IucHJvdG90eXBlLl92YWxpZGF0ZUZpZWxkID0gZnVuY3Rpb24oZmllbGQpIHtcbiAgICAgICAgdmFyIGksIGosXG4gICAgICAgICAgICBydWxlcyA9IGZpZWxkLnJ1bGVzLnNwbGl0KCd8JyksXG4gICAgICAgICAgICBpbmRleE9mUmVxdWlyZWQgPSBmaWVsZC5ydWxlcy5pbmRleE9mKCdyZXF1aXJlZCcpLFxuICAgICAgICAgICAgaXNFbXB0eSA9ICghZmllbGQudmFsdWUgfHwgZmllbGQudmFsdWUgPT09ICcnIHx8IGZpZWxkLnZhbHVlID09PSB1bmRlZmluZWQpO1xuXG4gICAgICAgIC8qXG4gICAgICAgICAqIFJ1biB0aHJvdWdoIHRoZSBydWxlcyBhbmQgZXhlY3V0ZSB0aGUgdmFsaWRhdGlvbiBtZXRob2RzIGFzIG5lZWRlZFxuICAgICAgICAgKi9cblxuICAgICAgICBmb3IgKGkgPSAwLCBydWxlTGVuZ3RoID0gcnVsZXMubGVuZ3RoOyBpIDwgcnVsZUxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICB2YXIgbWV0aG9kID0gcnVsZXNbaV0sXG4gICAgICAgICAgICAgICAgcGFyYW0gPSBudWxsLFxuICAgICAgICAgICAgICAgIGZhaWxlZCA9IGZhbHNlLFxuICAgICAgICAgICAgICAgIHBhcnRzID0gcnVsZVJlZ2V4LmV4ZWMobWV0aG9kKTtcblxuICAgICAgICAgICAgLypcbiAgICAgICAgICAgICAqIElmIHRoaXMgZmllbGQgaXMgbm90IHJlcXVpcmVkIGFuZCB0aGUgdmFsdWUgaXMgZW1wdHksIGNvbnRpbnVlIG9uIHRvIHRoZSBuZXh0IHJ1bGUgdW5sZXNzIGl0J3MgYSBjYWxsYmFjay5cbiAgICAgICAgICAgICAqIFRoaXMgZW5zdXJlcyB0aGF0IGEgY2FsbGJhY2sgd2lsbCBhbHdheXMgYmUgY2FsbGVkIGJ1dCBvdGhlciBydWxlcyB3aWxsIGJlIHNraXBwZWQuXG4gICAgICAgICAgICAgKi9cblxuICAgICAgICAgICAgaWYgKGluZGV4T2ZSZXF1aXJlZCA9PT0gLTEgJiYgbWV0aG9kLmluZGV4T2YoJyFjYWxsYmFja18nKSA9PT0gLTEgJiYgaXNFbXB0eSkge1xuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvKlxuICAgICAgICAgICAgICogSWYgdGhlIHJ1bGUgaGFzIGEgcGFyYW1ldGVyIChpLmUuIG1hdGNoZXNbcGFyYW1dKSBzcGxpdCBpdCBvdXRcbiAgICAgICAgICAgICAqL1xuXG4gICAgICAgICAgICBpZiAocGFydHMpIHtcbiAgICAgICAgICAgICAgICBtZXRob2QgPSBwYXJ0c1sxXTtcbiAgICAgICAgICAgICAgICBwYXJhbSA9IHBhcnRzWzJdO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAobWV0aG9kLmNoYXJBdCgwKSA9PT0gJyEnKSB7XG4gICAgICAgICAgICAgICAgbWV0aG9kID0gbWV0aG9kLnN1YnN0cmluZygxLCBtZXRob2QubGVuZ3RoKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLypcbiAgICAgICAgICAgICAqIElmIHRoZSBob29rIGlzIGRlZmluZWQsIHJ1biBpdCB0byBmaW5kIGFueSB2YWxpZGF0aW9uIGVycm9yc1xuICAgICAgICAgICAgICovXG5cbiAgICAgICAgICAgIGlmICh0eXBlb2YgdGhpcy5faG9va3NbbWV0aG9kXSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICAgIGlmICghdGhpcy5faG9va3NbbWV0aG9kXS5hcHBseSh0aGlzLCBbZmllbGQsIHBhcmFtXSkpIHtcbiAgICAgICAgICAgICAgICAgICAgZmFpbGVkID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2UgaWYgKG1ldGhvZC5zdWJzdHJpbmcoMCwgOSkgPT09ICdjYWxsYmFja18nKSB7XG4gICAgICAgICAgICAgICAgLy8gQ3VzdG9tIG1ldGhvZC4gRXhlY3V0ZSB0aGUgaGFuZGxlciBpZiBpdCB3YXMgcmVnaXN0ZXJlZFxuICAgICAgICAgICAgICAgIG1ldGhvZCA9IG1ldGhvZC5zdWJzdHJpbmcoOSwgbWV0aG9kLmxlbmd0aCk7XG5cbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHRoaXMuaGFuZGxlcnNbbWV0aG9kXSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5oYW5kbGVyc1ttZXRob2RdLmFwcGx5KHRoaXMsIFtmaWVsZC52YWx1ZSwgcGFyYW0sIGZpZWxkXSkgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBmYWlsZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvKlxuICAgICAgICAgICAgICogSWYgdGhlIGhvb2sgZmFpbGVkLCBhZGQgYSBtZXNzYWdlIHRvIHRoZSBlcnJvcnMgYXJyYXlcbiAgICAgICAgICAgICAqL1xuXG4gICAgICAgICAgICBpZiAoZmFpbGVkKSB7XG4gICAgICAgICAgICAgICAgLy8gTWFrZSBzdXJlIHdlIGhhdmUgYSBtZXNzYWdlIGZvciB0aGlzIHJ1bGVcbiAgICAgICAgICAgICAgICB2YXIgc291cmNlID0gdGhpcy5tZXNzYWdlc1tmaWVsZC5uYW1lICsgJy4nICsgbWV0aG9kXSB8fCB0aGlzLm1lc3NhZ2VzW21ldGhvZF0gfHwgZGVmYXVsdHMubWVzc2FnZXNbbWV0aG9kXSxcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZSA9ICdBbiBlcnJvciBoYXMgb2NjdXJyZWQgd2l0aCB0aGUgJyArIGZpZWxkLmRpc3BsYXkgKyAnIGZpZWxkLic7XG5cbiAgICAgICAgICAgICAgICBpZiAoc291cmNlKSB7XG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2UgPSBzb3VyY2UucmVwbGFjZSgnJXMnLCBmaWVsZC5kaXNwbGF5KTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAocGFyYW0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2UgPSBtZXNzYWdlLnJlcGxhY2UoJyVzJywgKHRoaXMuZmllbGRzW3BhcmFtXSkgPyB0aGlzLmZpZWxkc1twYXJhbV0uZGlzcGxheSA6IHBhcmFtKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHZhciBleGlzdGluZ0Vycm9yO1xuICAgICAgICAgICAgICAgIGZvciAoaiA9IDA7IGogPCB0aGlzLmVycm9ycy5sZW5ndGg7IGogKz0gMSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoZmllbGQuaWQgPT09IHRoaXMuZXJyb3JzW2pdLmlkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZ0Vycm9yID0gdGhpcy5lcnJvcnNbal07XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB2YXIgZXJyb3JPYmplY3QgPSBleGlzdGluZ0Vycm9yIHx8IHtcbiAgICAgICAgICAgICAgICAgICAgaWQ6IGZpZWxkLmlkLFxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmaWVsZC5kaXNwbGF5LFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50OiBmaWVsZC5lbGVtZW50LFxuICAgICAgICAgICAgICAgICAgICBuYW1lOiBmaWVsZC5uYW1lLFxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBtZXNzYWdlLFxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlczogW10sXG4gICAgICAgICAgICAgICAgICAgIHJ1bGU6IG1ldGhvZFxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgZXJyb3JPYmplY3QubWVzc2FnZXMucHVzaChtZXNzYWdlKTtcbiAgICAgICAgICAgICAgICBpZiAoIWV4aXN0aW5nRXJyb3IpIHRoaXMuZXJyb3JzLnB1c2goZXJyb3JPYmplY3QpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIHByaXZhdGUgZnVuY3Rpb24gX2dldFZhbGlkRGF0ZTogaGVscGVyIGZ1bmN0aW9uIHRvIGNvbnZlcnQgYSBzdHJpbmcgZGF0ZSB0byBhIERhdGUgb2JqZWN0XG4gICAgICogQHBhcmFtIGRhdGUgKFN0cmluZykgbXVzdCBiZSBpbiBmb3JtYXQgeXl5eS1tbS1kZCBvciB1c2Uga2V5d29yZDogdG9kYXlcbiAgICAgKiBAcmV0dXJucyB7RGF0ZX0gcmV0dXJucyBmYWxzZSBpZiBpbnZhbGlkXG4gICAgICovXG4gICAgRm9ybVZhbGlkYXRvci5wcm90b3R5cGUuX2dldFZhbGlkRGF0ZSA9IGZ1bmN0aW9uKGRhdGUpIHtcbiAgICAgICAgaWYgKCFkYXRlLm1hdGNoKCd0b2RheScpICYmICFkYXRlLm1hdGNoKGRhdGVSZWdleCkpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciB2YWxpZERhdGUgPSBuZXcgRGF0ZSgpLFxuICAgICAgICAgICAgdmFsaWREYXRlQXJyYXk7XG5cbiAgICAgICAgaWYgKCFkYXRlLm1hdGNoKCd0b2RheScpKSB7XG4gICAgICAgICAgICB2YWxpZERhdGVBcnJheSA9IGRhdGUuc3BsaXQoJy0nKTtcbiAgICAgICAgICAgIHZhbGlkRGF0ZS5zZXRGdWxsWWVhcih2YWxpZERhdGVBcnJheVswXSk7XG4gICAgICAgICAgICB2YWxpZERhdGUuc2V0TW9udGgodmFsaWREYXRlQXJyYXlbMV0gLSAxKTtcbiAgICAgICAgICAgIHZhbGlkRGF0ZS5zZXREYXRlKHZhbGlkRGF0ZUFycmF5WzJdKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdmFsaWREYXRlO1xuICAgIH07XG5cbiAgICAvKlxuICAgICAqIEBwcml2YXRlXG4gICAgICogT2JqZWN0IGNvbnRhaW5pbmcgYWxsIG9mIHRoZSB2YWxpZGF0aW9uIGhvb2tzXG4gICAgICovXG5cbiAgICBGb3JtVmFsaWRhdG9yLnByb3RvdHlwZS5faG9va3MgPSB7XG4gICAgICAgIHJlcXVpcmVkOiBmdW5jdGlvbihmaWVsZCkge1xuICAgICAgICAgICAgdmFyIHZhbHVlID0gZmllbGQudmFsdWU7XG5cbiAgICAgICAgICAgIGlmICgoZmllbGQudHlwZSA9PT0gJ2NoZWNrYm94JykgfHwgKGZpZWxkLnR5cGUgPT09ICdyYWRpbycpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIChmaWVsZC5jaGVja2VkID09PSB0cnVlKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuICh2YWx1ZSAhPT0gbnVsbCAmJiB2YWx1ZSAhPT0gJycpO1xuICAgICAgICB9LFxuXG4gICAgICAgIFwiZGVmYXVsdFwiOiBmdW5jdGlvbihmaWVsZCwgZGVmYXVsdE5hbWUpe1xuICAgICAgICAgICAgcmV0dXJuIGZpZWxkLnZhbHVlICE9PSBkZWZhdWx0TmFtZTtcbiAgICAgICAgfSxcblxuICAgICAgICBtYXRjaGVzOiBmdW5jdGlvbihmaWVsZCwgbWF0Y2hOYW1lKSB7XG4gICAgICAgICAgICB2YXIgZWwgPSB0aGlzLmZvcm1bbWF0Y2hOYW1lXTtcblxuICAgICAgICAgICAgaWYgKGVsKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpZWxkLnZhbHVlID09PSBlbC52YWx1ZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9LFxuXG4gICAgICAgIHZhbGlkX2VtYWlsOiBmdW5jdGlvbihmaWVsZCkge1xuICAgICAgICAgICAgcmV0dXJuIGVtYWlsUmVnZXgudGVzdChmaWVsZC52YWx1ZSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgdmFsaWRfZW1haWxzOiBmdW5jdGlvbihmaWVsZCkge1xuICAgICAgICAgICAgdmFyIHJlc3VsdCA9IGZpZWxkLnZhbHVlLnNwbGl0KC9cXHMqLFxccyovZyk7XG5cbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwLCByZXN1bHRMZW5ndGggPSByZXN1bHQubGVuZ3RoOyBpIDwgcmVzdWx0TGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBpZiAoIWVtYWlsUmVnZXgudGVzdChyZXN1bHRbaV0pKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9LFxuXG4gICAgICAgIG1pbl9sZW5ndGg6IGZ1bmN0aW9uKGZpZWxkLCBsZW5ndGgpIHtcbiAgICAgICAgICAgIGlmICghbnVtZXJpY1JlZ2V4LnRlc3QobGVuZ3RoKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIChmaWVsZC52YWx1ZS5sZW5ndGggPj0gcGFyc2VJbnQobGVuZ3RoLCAxMCkpO1xuICAgICAgICB9LFxuXG4gICAgICAgIG1heF9sZW5ndGg6IGZ1bmN0aW9uKGZpZWxkLCBsZW5ndGgpIHtcbiAgICAgICAgICAgIGlmICghbnVtZXJpY1JlZ2V4LnRlc3QobGVuZ3RoKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIChmaWVsZC52YWx1ZS5sZW5ndGggPD0gcGFyc2VJbnQobGVuZ3RoLCAxMCkpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGV4YWN0X2xlbmd0aDogZnVuY3Rpb24oZmllbGQsIGxlbmd0aCkge1xuICAgICAgICAgICAgaWYgKCFudW1lcmljUmVnZXgudGVzdChsZW5ndGgpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gKGZpZWxkLnZhbHVlLmxlbmd0aCA9PT0gcGFyc2VJbnQobGVuZ3RoLCAxMCkpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGdyZWF0ZXJfdGhhbjogZnVuY3Rpb24oZmllbGQsIHBhcmFtKSB7XG4gICAgICAgICAgICBpZiAoIWRlY2ltYWxSZWdleC50ZXN0KGZpZWxkLnZhbHVlKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIChwYXJzZUZsb2F0KGZpZWxkLnZhbHVlKSA+IHBhcnNlRmxvYXQocGFyYW0pKTtcbiAgICAgICAgfSxcblxuICAgICAgICBsZXNzX3RoYW46IGZ1bmN0aW9uKGZpZWxkLCBwYXJhbSkge1xuICAgICAgICAgICAgaWYgKCFkZWNpbWFsUmVnZXgudGVzdChmaWVsZC52YWx1ZSkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiAocGFyc2VGbG9hdChmaWVsZC52YWx1ZSkgPCBwYXJzZUZsb2F0KHBhcmFtKSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgYWxwaGE6IGZ1bmN0aW9uKGZpZWxkKSB7XG4gICAgICAgICAgICByZXR1cm4gKGFscGhhUmVnZXgudGVzdChmaWVsZC52YWx1ZSkpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGFscGhhX251bWVyaWM6IGZ1bmN0aW9uKGZpZWxkKSB7XG4gICAgICAgICAgICByZXR1cm4gKGFscGhhTnVtZXJpY1JlZ2V4LnRlc3QoZmllbGQudmFsdWUpKTtcbiAgICAgICAgfSxcblxuICAgICAgICBhbHBoYV9kYXNoOiBmdW5jdGlvbihmaWVsZCkge1xuICAgICAgICAgICAgcmV0dXJuIChhbHBoYURhc2hSZWdleC50ZXN0KGZpZWxkLnZhbHVlKSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgbnVtZXJpYzogZnVuY3Rpb24oZmllbGQpIHtcbiAgICAgICAgICAgIHJldHVybiAobnVtZXJpY1JlZ2V4LnRlc3QoZmllbGQudmFsdWUpKTtcbiAgICAgICAgfSxcblxuICAgICAgICBpbnRlZ2VyOiBmdW5jdGlvbihmaWVsZCkge1xuICAgICAgICAgICAgcmV0dXJuIChpbnRlZ2VyUmVnZXgudGVzdChmaWVsZC52YWx1ZSkpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGRlY2ltYWw6IGZ1bmN0aW9uKGZpZWxkKSB7XG4gICAgICAgICAgICByZXR1cm4gKGRlY2ltYWxSZWdleC50ZXN0KGZpZWxkLnZhbHVlKSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaXNfbmF0dXJhbDogZnVuY3Rpb24oZmllbGQpIHtcbiAgICAgICAgICAgIHJldHVybiAobmF0dXJhbFJlZ2V4LnRlc3QoZmllbGQudmFsdWUpKTtcbiAgICAgICAgfSxcblxuICAgICAgICBpc19uYXR1cmFsX25vX3plcm86IGZ1bmN0aW9uKGZpZWxkKSB7XG4gICAgICAgICAgICByZXR1cm4gKG5hdHVyYWxOb1plcm9SZWdleC50ZXN0KGZpZWxkLnZhbHVlKSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgdmFsaWRfaXA6IGZ1bmN0aW9uKGZpZWxkKSB7XG4gICAgICAgICAgICByZXR1cm4gKGlwUmVnZXgudGVzdChmaWVsZC52YWx1ZSkpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHZhbGlkX2Jhc2U2NDogZnVuY3Rpb24oZmllbGQpIHtcbiAgICAgICAgICAgIHJldHVybiAoYmFzZTY0UmVnZXgudGVzdChmaWVsZC52YWx1ZSkpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHZhbGlkX3VybDogZnVuY3Rpb24oZmllbGQpIHtcbiAgICAgICAgICAgIHJldHVybiAodXJsUmVnZXgudGVzdChmaWVsZC52YWx1ZSkpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHZhbGlkX2NyZWRpdF9jYXJkOiBmdW5jdGlvbihmaWVsZCl7XG4gICAgICAgICAgICAvLyBMdWhuIENoZWNrIENvZGUgZnJvbSBodHRwczovL2dpc3QuZ2l0aHViLmNvbS80MDc1NTMzXG4gICAgICAgICAgICAvLyBhY2NlcHQgb25seSBkaWdpdHMsIGRhc2hlcyBvciBzcGFjZXNcbiAgICAgICAgICAgIGlmICghbnVtZXJpY0Rhc2hSZWdleC50ZXN0KGZpZWxkLnZhbHVlKSkgcmV0dXJuIGZhbHNlO1xuXG4gICAgICAgICAgICAvLyBUaGUgTHVobiBBbGdvcml0aG0uIEl0J3Mgc28gcHJldHR5LlxuICAgICAgICAgICAgdmFyIG5DaGVjayA9IDAsIG5EaWdpdCA9IDAsIGJFdmVuID0gZmFsc2U7XG4gICAgICAgICAgICB2YXIgc3RyaXBwZWRGaWVsZCA9IGZpZWxkLnZhbHVlLnJlcGxhY2UoL1xcRC9nLCBcIlwiKTtcblxuICAgICAgICAgICAgZm9yICh2YXIgbiA9IHN0cmlwcGVkRmllbGQubGVuZ3RoIC0gMTsgbiA+PSAwOyBuLS0pIHtcbiAgICAgICAgICAgICAgICB2YXIgY0RpZ2l0ID0gc3RyaXBwZWRGaWVsZC5jaGFyQXQobik7XG4gICAgICAgICAgICAgICAgbkRpZ2l0ID0gcGFyc2VJbnQoY0RpZ2l0LCAxMCk7XG4gICAgICAgICAgICAgICAgaWYgKGJFdmVuKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICgobkRpZ2l0ICo9IDIpID4gOSkgbkRpZ2l0IC09IDk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgbkNoZWNrICs9IG5EaWdpdDtcbiAgICAgICAgICAgICAgICBiRXZlbiA9ICFiRXZlbjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIChuQ2hlY2sgJSAxMCkgPT09IDA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaXNfZmlsZV90eXBlOiBmdW5jdGlvbihmaWVsZCx0eXBlKSB7XG4gICAgICAgICAgICBpZiAoZmllbGQudHlwZSAhPT0gJ2ZpbGUnKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHZhciBleHQgPSBmaWVsZC52YWx1ZS5zdWJzdHIoKGZpZWxkLnZhbHVlLmxhc3RJbmRleE9mKCcuJykgKyAxKSksXG4gICAgICAgICAgICAgICAgdHlwZUFycmF5ID0gdHlwZS5zcGxpdCgnLCcpLFxuICAgICAgICAgICAgICAgIGluQXJyYXkgPSBmYWxzZSxcbiAgICAgICAgICAgICAgICBpID0gMCxcbiAgICAgICAgICAgICAgICBsZW4gPSB0eXBlQXJyYXkubGVuZ3RoO1xuXG4gICAgICAgICAgICBmb3IgKGk7IGkgPCBsZW47IGkrKykge1xuICAgICAgICAgICAgICAgIGlmIChleHQudG9VcHBlckNhc2UoKSA9PSB0eXBlQXJyYXlbaV0udG9VcHBlckNhc2UoKSkgaW5BcnJheSA9IHRydWU7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBpbkFycmF5O1xuICAgICAgICB9LFxuXG4gICAgICAgIGdyZWF0ZXJfdGhhbl9kYXRlOiBmdW5jdGlvbiAoZmllbGQsIGRhdGUpIHtcbiAgICAgICAgICAgIHZhciBlbnRlcmVkRGF0ZSA9IHRoaXMuX2dldFZhbGlkRGF0ZShmaWVsZC52YWx1ZSksXG4gICAgICAgICAgICAgICAgdmFsaWREYXRlID0gdGhpcy5fZ2V0VmFsaWREYXRlKGRhdGUpO1xuXG4gICAgICAgICAgICBpZiAoIXZhbGlkRGF0ZSB8fCAhZW50ZXJlZERhdGUpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBlbnRlcmVkRGF0ZSA+IHZhbGlkRGF0ZTtcbiAgICAgICAgfSxcblxuICAgICAgICBsZXNzX3RoYW5fZGF0ZTogZnVuY3Rpb24gKGZpZWxkLCBkYXRlKSB7XG4gICAgICAgICAgICB2YXIgZW50ZXJlZERhdGUgPSB0aGlzLl9nZXRWYWxpZERhdGUoZmllbGQudmFsdWUpLFxuICAgICAgICAgICAgICAgIHZhbGlkRGF0ZSA9IHRoaXMuX2dldFZhbGlkRGF0ZShkYXRlKTtcblxuICAgICAgICAgICAgaWYgKCF2YWxpZERhdGUgfHwgIWVudGVyZWREYXRlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gZW50ZXJlZERhdGUgPCB2YWxpZERhdGU7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ3JlYXRlcl90aGFuX29yX2VxdWFsX2RhdGU6IGZ1bmN0aW9uIChmaWVsZCwgZGF0ZSkge1xuICAgICAgICAgICAgdmFyIGVudGVyZWREYXRlID0gdGhpcy5fZ2V0VmFsaWREYXRlKGZpZWxkLnZhbHVlKSxcbiAgICAgICAgICAgICAgICB2YWxpZERhdGUgPSB0aGlzLl9nZXRWYWxpZERhdGUoZGF0ZSk7XG5cbiAgICAgICAgICAgIGlmICghdmFsaWREYXRlIHx8ICFlbnRlcmVkRGF0ZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGVudGVyZWREYXRlID49IHZhbGlkRGF0ZTtcbiAgICAgICAgfSxcblxuICAgICAgICBsZXNzX3RoYW5fb3JfZXF1YWxfZGF0ZTogZnVuY3Rpb24gKGZpZWxkLCBkYXRlKSB7XG4gICAgICAgICAgICB2YXIgZW50ZXJlZERhdGUgPSB0aGlzLl9nZXRWYWxpZERhdGUoZmllbGQudmFsdWUpLFxuICAgICAgICAgICAgICAgIHZhbGlkRGF0ZSA9IHRoaXMuX2dldFZhbGlkRGF0ZShkYXRlKTtcblxuICAgICAgICAgICAgaWYgKCF2YWxpZERhdGUgfHwgIWVudGVyZWREYXRlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gZW50ZXJlZERhdGUgPD0gdmFsaWREYXRlO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIHdpbmRvdy5Gb3JtVmFsaWRhdG9yID0gRm9ybVZhbGlkYXRvcjtcbn0pKHdpbmRvdywgZG9jdW1lbnQpO1xuXG4vKlxuICogRXhwb3J0IGFzIGEgQ29tbW9uSlMgbW9kdWxlXG4gKi9cbmlmICh0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJyAmJiBtb2R1bGUuZXhwb3J0cykge1xuICAgIG1vZHVsZS5leHBvcnRzID0gRm9ybVZhbGlkYXRvcjtcbn1cbiJdfQ==
