<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

ini_set('session.cookie_lifetime', '864000'); // ten days in seconds

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';

/**
 * Error and Exception handling
 */

// Activer ou desactiver les erreurs PHP
// Custom Simon
ini_set('display_errors', 1);
// Enregistrer les erreurs dans un fichier de log
ini_set('log_errors', 1);
error_reporting(E_ALL);

/* A reactiver d'origine si besoin
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');
*/

/**
 * Sessions
 */
session_start();

/**
 * Routing
 */
$router = new Core\Router();

// Affiche la page d'accueil non connecte (sans menu)
$router->add('', ['controller' => 'Home', 'action' => 'index']);

/*
Publicite professionnel
Grade particulier = OK
RAF POST = OK / 
PUBLIER ARTICLES / 
filtres findup, distances, categories, nombre commentairen, annonce pro, annonce particulier / 
BO - ADMIN ET PRO / 
REDACTIONS CGU */

/******************** GESTION PARTICULIER ********************/
// Affiche le formualire de connexion
$router->add('connexion/particulier', ['controller' => 'Connexion', 'action' => 'particulier']);
// Affiche le back-office lors de l'identification
//$router->add('connexion/particulier/compte', ['controller' => 'Connexion', 'action' => 'particulierConnect']);

// Routeur permet de rediriger le role sur la bonne vue et de tester si il est connecter
$router->add('connexion/particulier/compte', ['controller' => 'Connexion', 'action' => 'routerRoleConnect']);

// Grade - page d'accueil du back-office
$router->add('connexion/particulier/grade', ['controller' => 'Connexion', 'action' => 'particulierGrade']);

// Permet la modifications des donnes de l'utilisateur
$router->add('connexion/particulier/compte/informations', ['controller' => 'Informations', 'action' => 'particulierForm']);
// Valide et UPDATE les DATA en BDD
$router->add('connexion/particulier/compte/informations/validate', ['controller' => 'Informations', 'action' => 'particulierRecupFormUpdate']);

// Gestion ajout d'une annonce (Affiche le formulaire)
$router->add('connexion/particulier/post/annonce', ['controller' => 'Post', 'action' => 'particulier']);

// Gestion de la publicaton d'une annonce
$router->add('connexion/particulier/post/annonce/add', ['controller' => 'Post', 'action' => 'particulierAdd']);

// Gestion de l'affichage d'une annonce
$router->add('connexion/particulier/show/annonce/categories', ['controller' => 'Ads', 'action' => 'showCategories']);

// Gestion de l'affichage d'une annonce
$router->add('connexion/particulier/show/annonce/categories/annonces', ['controller' => 'Ads', 'action' => 'showAnnonces']);

// Gestion de l'affichage d'une annonce avec des filtres
$router->add('connexion/particulier/show/annonce/categories/annonces/filtre', ['controller' => 'Ads', 'action' => 'showWithFiltre']);

// Gestion de l'affichage d'une annonce avec des filtres
$router->add('connexion/particulier/show/annonce/categories/annonces/update/findup', ['controller' => 'Ads', 'action' => 'updateFindUp']);

// Permet la deconnexion
$router->add('connexion/particulier/deconnexion', ['controller' => 'Connexion', 'action' => 'deconnexion']);




/** Gestion admin **/








/** Gestion professionnel **/
// Connexion a custom
$router->add('connexion/', ['controller' => 'Connexion', 'action' => 'professionnel']);

// Routeur permet de rediriger le role sur la bonne vue et de tester si il est connecter
$router->add('connexion/professionnel/compte', ['controller' => 'Connexion', 'action' => 'routerRoleConnect']);

// A terminer









// A Purger


$router->add('logout', ['controller' => 'Login', 'action' => 'destroy']);

$router->add('password/reset/{token:[\da-f]+}', ['controller' => 'Password', 'action' => 'reset']);
$router->add('signup/activate/{token:[\da-f]+}', ['controller' => 'Signup', 'action' => 'activate']);

// Posts
$router->add('posts', ['controller' => 'Posts', 'action' => 'index']);
$router->add('posts/new', ['controller' => 'Posts', 'action' => 'new']);





$router->add('{controller}/{action}');
$router->add('{controller}/{id}/{action}');
$router->add('{controller}/{id:\d+}/{action}');
$router->add('admin/{controller}/{action}', ['namespace' => 'Admin']);

$router->dispatch($_SERVER['QUERY_STRING']);