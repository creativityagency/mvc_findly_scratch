#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Utilisateurs
#------------------------------------------------------------

CREATE TABLE Utilisateurs(
        id       int (11) Auto_increment  NOT NULL ,
        nom      Varchar (50) ,
        prenom   Varchar (50) ,
        mail     Varchar (100) ,
        password Varchar (150) ,
        role     Varchar (25) ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: annonces
#------------------------------------------------------------

CREATE TABLE annonces(
        titre             Varchar (50) ,
        contenus          Varchar (250) ,
        type_annonce      Varchar (25) ,
        heure_publication Date ,
        photo_annonce     Varchar (250) ,
        id                int (11) Auto_increment  NOT NULL ,
        id_Categories     Int ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Commercant
#------------------------------------------------------------

CREATE TABLE Commercant(
        id              int (11) Auto_increment  NOT NULL ,
        nom_entreprise  Varchar (50) ,
        nom_responsable Varchar (25) ,
        ville           Varchar (25) ,
        adresse         Varchar (200) ,
        code_postal     Varchar (25) ,
        offres_proposes Varchar (25) ,
        duree_de_offre  Int ,
        offre_paye      Bool ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Categories
#------------------------------------------------------------

CREATE TABLE Categories(
        id  Int NOT NULL ,
        nom Varchar (75) ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: infos_annonces_particuliers
#------------------------------------------------------------

CREATE TABLE infos_annonces_particuliers(
        comentaires Varchar (500) ,
        findup      Int ,
        id          Int NOT NULL ,
        id_annonces Int NOT NULL ,
        PRIMARY KEY (id ,id_annonces )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: infos_annonces_pro
#------------------------------------------------------------

CREATE TABLE infos_annonces_pro(
        commentaires Varchar (250) ,
        findup       Int ,
        id           Int NOT NULL ,
        id_annonces  Int NOT NULL ,
        PRIMARY KEY (id ,id_annonces )
)ENGINE=InnoDB;

ALTER TABLE annonces ADD CONSTRAINT FK_annonces_id_Categories FOREIGN KEY (id_Categories) REFERENCES Categories(id);
ALTER TABLE infos_annonces_particuliers ADD CONSTRAINT FK_infos_annonces_particuliers_id FOREIGN KEY (id) REFERENCES Utilisateurs(id);
ALTER TABLE infos_annonces_particuliers ADD CONSTRAINT FK_infos_annonces_particuliers_id_annonces FOREIGN KEY (id_annonces) REFERENCES annonces(id);
ALTER TABLE infos_annonces_pro ADD CONSTRAINT FK_infos_annonces_pro_id FOREIGN KEY (id) REFERENCES Commercant(id);
ALTER TABLE infos_annonces_pro ADD CONSTRAINT FK_infos_annonces_pro_id_annonces FOREIGN KEY (id_annonces) REFERENCES annonces(id);
