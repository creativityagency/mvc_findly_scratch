-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  sam. 31 mars 2018 à 16:04
-- Version du serveur :  5.7.19
-- Version de PHP :  7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `findly`
--

--
-- Déchargement des données de la table `annonces`
--

INSERT INTO `annonces` (`titre`, `contenus`, `type_annonce`, `heure_publication`, `photo_annonce`, `id`, `id_Categories`) VALUES
('test_annonce_1', 'TEST_CONTENU', 'particulier', '2018-03-31', 'link_en_cours', 2, 1),
('TEST_TITRE_2', 'TEST_CONTENUS_2', 'particulier', '2018-03-31', 'TEST_EN_COURS', 3, 1),
('TEST_TITRE_3', 'CONTENU_3', 'pro', '2018-03-31', 'link_en_cours', 4, 1),
('test', 'test', 'particulier', '2018-03-31', 'link', 5, 1),
('test5', 'test5', 'particulier', '2018-03-30', 'link', 6, 1),
('test6', 'test6', 'pro', '2018-03-30', 'link', 7, 1),
('test7', 'test7', 'pro', '2018-03-31', NULL, 8, NULL);

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`) VALUES
(1, 'Sortis'),
(2, 'Voyages');

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `nom`, `prenom`, `mail`, `password`, `role`) VALUES
(1, 'lpdw', 'simon', 'rugor.simon.sio@gmail.com', '63a9f0ea7bb98050796b649e85481845', 'ROLE_ADMIN');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
