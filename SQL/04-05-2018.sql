-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 04 mai 2018 à 10:15
-- Version du serveur :  5.7.19
-- Version de PHP :  7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `findly`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonces`
--

DROP TABLE IF EXISTS `annonces`;
CREATE TABLE IF NOT EXISTS `annonces` (
  `titre` varchar(50) DEFAULT NULL,
  `contenus` text,
  `type_annonce` varchar(25) DEFAULT NULL,
  `date_publication` date DEFAULT NULL,
  `photo_annonce` varchar(250) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Categories` int(11) DEFAULT NULL,
  `nom` varchar(150) DEFAULT NULL,
  `prenom` varchar(150) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `find_up` int(11) DEFAULT '0',
  `remontes` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_annonces_id_Categories` (`id_Categories`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `annonces`
--

INSERT INTO `annonces` (`titre`, `contenus`, `type_annonce`, `date_publication`, `photo_annonce`, `id`, `id_Categories`, `nom`, `prenom`, `email`, `find_up`, `remontes`) VALUES
('Bowling ', 'Nous vous accueillons chaleureusement dans un vaste espace dédié à la détente et aux loisirs. Venez découvrir notre carte et dégustez nos nombreux plats dans le cadre chic et cosy de notre bowling-restaurant.\r\n', 'ROLE_PRO', '2018-04-27', '/findly/public/src/upload/up-part2223particulier@gmail.com27-04-201817-39-37.jpg', 51, 1, 'part2223', 'part22', 'particulier@gmail.com', 18, NULL),
('_r', 'iyt', 'ROLE_PARTICULIER', '2018-04-27', '/findly/public/src/upload/up-part2223particulier@gmail.com27-04-201817-54-50.png', 52, 1, 'part2223', 'part22', 'particulier@gmail.com', 5, NULL),
('ferrari', 'iuti', 'ROLE_PARTICULIER', '2018-05-27', '/findly/public/src/upload/up-part2223particulier@gmail.com27-04-201818-20-00.jpg', 53, 1, 'part2223', 'part22', 'particulier@gmail.com', 6, NULL),
('fgsfddfh', 't', 'ROLE_PARTICULIER', '2018-04-27', '/findly/public/src/upload/up-part2223particulier@gmail.com27-04-201818-34-49.jpg', 54, 2, 'part2223', 'part22', 'particulier@gmail.com', 6, NULL),
('rtre', 'ret', 'ROLE_PARTICULIER', '2018-04-27', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com27-04-201818-35-14.jpg', 55, 3, 'part2223', 'part22', 'particulier@gmail.com', 0, NULL),
('ez', 'ez', 'ROLE_PARTICULIER', '2018-04-28', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com28-04-201818-34-02.png', 56, 3, 'part2223', 'part22', 'particulier@gmail.com', 1, NULL),
('klefh', 'glh', 'ROLE_PARTICULIER', '2018-05-02', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com02-05-201807-40-44.jpg', 57, 2, 'part2223', 'part22', 'particulier@gmail.com', 0, NULL),
('test', 'trest', 'ROLE_PARTICULIER', '2018-05-02', '/findly/public/src/upload/up-part2223particulier@gmail.com02-05-201808-22-14.jpg', 58, 3, 'part2223', 'part22', 'particulier@gmail.com', 0, NULL),
('testtest', 'test2', 'ROLE_PARTICULIER', '2018-05-02', '/findly/public/src/upload/up-part2223particulier@gmail.com27-04-201817-39-37.jpg', 59, 1, 'part2223', 'part22', 'particulier@gmail.com', 1, NULL),
('test3', 'test3', 'ROLE_PARTICULIER', '2018-05-02', '/findly/public/src/upload/up-part2223particulier@gmail.com02-05-201808-53-37.jpg', 60, 1, 'part2223', 'part22', 'particulier@gmail.com', 0, NULL),
('test', 'test4', 'ROLE_PARTICULIER', '2018-05-02', '/findly/public/src/upload/up-part2223particulier@gmail.com02-05-201808-57-03.jpg', 61, 1, 'part2223', 'part22', 'particulier@gmail.com', 0, NULL),
('ç', '_', 'ROLE_PARTICULIER', '2018-05-02', '/findly/public/src/upload/up-part2223particulier@gmail.com02-05-201808-58-14.jpg', 62, 1, 'part2223', 'part22', 'particulier@gmail.com', 0, NULL),
('test6', 'tets6', 'ROLE_PARTICULIER', '2018-05-02', '/findly/public/src/upload/up-part2223particulier@gmail.com02-05-201809-19-42.jpg', 71, 1, 'part2223', 'part22', 'particulier@gmail.com', 1, NULL),
('titre6', 'titre6', 'ROLE_PARTICULIER', '2018-05-02', '/findly/public/src/upload/up-part2223particulier@gmail.com02-05-201809-56-41.jpg', 72, 1, 'part2223', 'part22', 'particulier@gmail.com', 0, NULL),
('test7', 'test7', 'ROLE_PARTICULIER', '2018-05-02', '/findly/public/src/upload/up-part2223particulier@gmail.com02-05-201809-57-16.jpg', 73, 1, 'part2223', 'part22', 'particulier@gmail.com', 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `nom` varchar(75) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`, `name`) VALUES
(1, 'Sortis', 'sortis'),
(2, 'Voyages', 'voyages'),
(3, 'Job/Emploi', 'emplois');

-- --------------------------------------------------------

--
-- Structure de la table `commercant`
--

DROP TABLE IF EXISTS `commercant`;
CREATE TABLE IF NOT EXISTS `commercant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_entreprise` varchar(50) DEFAULT NULL,
  `nom_responsable` varchar(25) DEFAULT NULL,
  `ville` varchar(25) DEFAULT NULL,
  `adresse` varchar(200) DEFAULT NULL,
  `code_postal` varchar(25) DEFAULT NULL,
  `offres_proposes` varchar(25) DEFAULT NULL,
  `duree_de_offre` int(11) DEFAULT NULL,
  `offre_paye` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `infos_annonces_particuliers`
--

DROP TABLE IF EXISTS `infos_annonces_particuliers`;
CREATE TABLE IF NOT EXISTS `infos_annonces_particuliers` (
  `comentaires` varchar(500) DEFAULT NULL,
  `findup` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `id_annonces` int(11) NOT NULL,
  `nom` varchar(200) DEFAULT NULL,
  `prenom` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`,`id_annonces`),
  KEY `FK_infos_annonces_particuliers_id_annonces` (`id_annonces`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `infos_annonces_pro`
--

DROP TABLE IF EXISTS `infos_annonces_pro`;
CREATE TABLE IF NOT EXISTS `infos_annonces_pro` (
  `commentaires` varchar(250) DEFAULT NULL,
  `findup` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `id_annonces` int(11) NOT NULL,
  PRIMARY KEY (`id`,`id_annonces`),
  KEY `FK_infos_annonces_pro_id_annonces` (`id_annonces`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `professionnel`
--

DROP TABLE IF EXISTS `professionnel`;
CREATE TABLE IF NOT EXISTS `professionnel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_entreprise` varchar(50) DEFAULT NULL,
  `nom_responsable` varchar(25) DEFAULT NULL,
  `ville` varchar(25) DEFAULT NULL,
  `adresse` varchar(200) DEFAULT NULL,
  `code_postal` varchar(25) DEFAULT NULL,
  `prenom_responsable` varchar(200) DEFAULT NULL,
  `mail` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `grade` int(11) DEFAULT NULL,
  `connected` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `professionnel`
--

INSERT INTO `professionnel` (`id`, `nom_entreprise`, `nom_responsable`, `ville`, `adresse`, `code_postal`, `prenom_responsable`, `mail`, `password`, `role`, `grade`, `connected`) VALUES
(1, 'microsoft', 'simon', 'montfermeil', 'x', '93370', 'prenom', 'pro@gmail.com', '99adc231b045331e514a516b4b7680f588e3823213abe901738bc3ad67b2f6fcb3c64efb93d18002588d3ccc1a49efbae1ce20cb43df36b38651f11fa75678e8', 'ROLE_PRO', 20, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `mail` varchar(100) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `role` varchar(25) DEFAULT NULL,
  `date_naissance` date DEFAULT NULL,
  `compte_active` tinyint(1) DEFAULT NULL,
  `grade` int(11) DEFAULT NULL,
  `connected` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `nom`, `prenom`, `mail`, `password`, `role`, `date_naissance`, `compte_active`, `grade`, `connected`) VALUES
(1, 'part22', 'part22', 'rugor.simon.sio@gmail.com', '99adc231b045331e514a516b4b7680f588e3823213abe901738bc3ad67b2f6fcb3c64efb93d18002588d3ccc1a49efbae1ce20cb43df36b38651f11fa75678e8', 'ROLE_ADMIN', '1980-12-25', 2, 2, NULL),
(2, 'part2223', 'part22', 'particulier@gmail.com', 'a144a1194c8eb46bf92087ac205bac9a6e6f50d1c19eb68af143c6eb63e9c6601a29a9c1684a16d5b51748ac5e188fb1686e3b9227442c41a12672b704ad44bd', 'ROLE_PARTICULIER', '1980-12-25', 5, 94, NULL),
(4, 'part22', 'part22', 'particulier2@gmail.com', 'a144a1194c8eb46bf92087ac205bac9a6e6f50d1c19eb68af143c6eb63e9c6601a29a9c1684a16d5b51748ac5e188fb1686e3b9227442c41a12672b704ad44bd', 'ROLE_PARTICULIER', '1980-12-25', 1, 20, NULL),
(5, 'pro', 'pro', 'pro@gmail.com', '99adc231b045331e514a516b4b7680f588e3823213abe901738bc3ad67b2f6fcb3c64efb93d18002588d3ccc1a49efbae1ce20cb43df36b38651f11fa75678e8', 'ROLE_PRO', '2018-04-10', 1, 2, NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `annonces`
--
ALTER TABLE `annonces`
  ADD CONSTRAINT `FK_annonces_id_Categories` FOREIGN KEY (`id_Categories`) REFERENCES `categories` (`id`);

--
-- Contraintes pour la table `infos_annonces_particuliers`
--
ALTER TABLE `infos_annonces_particuliers`
  ADD CONSTRAINT `FK_infos_annonces_particuliers_id` FOREIGN KEY (`id`) REFERENCES `utilisateurs` (`id`),
  ADD CONSTRAINT `FK_infos_annonces_particuliers_id_annonces` FOREIGN KEY (`id_annonces`) REFERENCES `annonces` (`id`);

--
-- Contraintes pour la table `infos_annonces_pro`
--
ALTER TABLE `infos_annonces_pro`
  ADD CONSTRAINT `FK_infos_annonces_pro_id` FOREIGN KEY (`id`) REFERENCES `commercant` (`id`),
  ADD CONSTRAINT `FK_infos_annonces_pro_id_annonces` FOREIGN KEY (`id_annonces`) REFERENCES `annonces` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
