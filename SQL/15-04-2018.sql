-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 15 avr. 2018 à 14:21
-- Version du serveur :  5.7.19
-- Version de PHP :  7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `findly`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonces`
--

DROP TABLE IF EXISTS `annonces`;
CREATE TABLE IF NOT EXISTS `annonces` (
  `titre` varchar(50) DEFAULT NULL,
  `contenus` varchar(250) DEFAULT NULL,
  `type_annonce` varchar(25) DEFAULT NULL,
  `date_publication` date DEFAULT NULL,
  `photo_annonce` varchar(250) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Categories` int(11) DEFAULT NULL,
  `find_up` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_annonces_id_Categories` (`id_Categories`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `annonces`
--

INSERT INTO `annonces` (`titre`, `contenus`, `type_annonce`, `date_publication`, `photo_annonce`, `id`, `id_Categories`, `find_up`, `date`) VALUES
('test_annonce_1', 'TEST_CONTENU', 'particulier', '2018-03-31', 'link_en_cours', 2, 1, NULL, NULL),
('TEST_TITRE_2', 'TEST_CONTENUS_2', 'particulier', '2018-03-31', 'TEST_EN_COURS', 3, 1, NULL, NULL),
('TEST_TITRE_3', 'CONTENU_3', 'pro', '2018-03-31', 'link_en_cours', 4, 1, NULL, NULL),
('test', 'test', 'particulier', '2018-03-31', 'link', 5, 1, NULL, NULL),
('test5', 'test5', 'particulier', '2018-03-30', 'link', 6, 1, NULL, NULL),
('test6', 'test6', 'pro', '2018-03-30', 'link', 7, 1, NULL, NULL),
('test7', 'test7', 'pro', '2018-03-31', NULL, 8, NULL, NULL, NULL),
('k', 'k', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201813-36-42.jpeg', 9, 1, NULL, NULL),
('k', 'k', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201813-37-44.jpeg', 10, 1, NULL, NULL),
('k', 'k', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201813-37-44.jpeg', 11, 1, NULL, NULL),
('k', 'l', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201813-49-12.jpeg', 12, 1, NULL, NULL),
('k', 'l', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201813-49-12.jpeg', 13, 1, NULL, NULL),
('kKKK', 'lm', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201813-53-22.jpeg', 14, 1, NULL, NULL),
('kKKK', 'lm', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201813-53-22.jpeg', 15, 1, NULL, NULL),
('oo', 'oo', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201813-54-21.jpeg', 16, 3, NULL, NULL),
('oo', 'oo', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201813-54-21.jpeg', 17, 3, NULL, NULL),
('mon', 'mon', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201813-56-12.jpg', 18, 2, NULL, NULL),
('mon', 'mon', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201813-56-12.jpg', 19, 2, NULL, NULL),
('test', 'test', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201813-59-21.jpg', 20, 2, NULL, NULL),
('test', 'test', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201813-59-21.jpg', 21, 2, NULL, NULL),
('test2', 'test2', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part22particulier2@gmail.com15-04-201814-01-20.png', 22, 1, NULL, NULL),
('test2', 'test2', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part22particulier2@gmail.com15-04-201814-01-20.png', 23, 1, NULL, NULL),
('test3', 'tesy3', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201814-03-10.png', 24, 1, NULL, NULL),
('test3', 'tesy3', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201814-03-10.png', 25, 1, NULL, NULL),
('test3', 'test3', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201814-04-28.png', 26, 1, NULL, NULL),
('test3', 'test3', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201814-04-28.png', 27, 1, NULL, NULL),
('test4', 'test4', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201814-05-16.png', 28, 1, NULL, NULL),
('test4', 'test4', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201814-05-16.png', 29, 1, NULL, NULL),
('simonnn', 'simonn', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part22particulier2@gmail.com15-04-201814-06-45.jpeg', 30, 1, NULL, NULL),
('simonnn', 'simonn', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part22particulier2@gmail.com15-04-201814-06-45.jpeg', 31, 1, NULL, NULL),
('simmmmmmmmm', 'iyohohgomh', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201814-07-49.jpg', 32, 1, NULL, NULL),
('simmmmmmmmm', 'iyohohgomh', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part2223particulier@gmail.com15-04-201814-07-49.jpg', 33, 1, NULL, NULL),
('testfin', 'fin', 'ROLE_PARTICULIER', '2018-04-15', 'C:\\wamp64\\www\\findly\\public\\src\\upload\\up-part22particulier2@gmail.com15-04-201814-10-48.png', 34, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `nom` varchar(75) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`, `name`) VALUES
(1, 'Sortis', 'sortis'),
(2, 'Voyages', 'voyages'),
(3, 'Job/Emploi', 'emplois');

-- --------------------------------------------------------

--
-- Structure de la table `commercant`
--

DROP TABLE IF EXISTS `commercant`;
CREATE TABLE IF NOT EXISTS `commercant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_entreprise` varchar(50) DEFAULT NULL,
  `nom_responsable` varchar(25) DEFAULT NULL,
  `ville` varchar(25) DEFAULT NULL,
  `adresse` varchar(200) DEFAULT NULL,
  `code_postal` varchar(25) DEFAULT NULL,
  `offres_proposes` varchar(25) DEFAULT NULL,
  `duree_de_offre` int(11) DEFAULT NULL,
  `offre_paye` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `infos_annonces_particuliers`
--

DROP TABLE IF EXISTS `infos_annonces_particuliers`;
CREATE TABLE IF NOT EXISTS `infos_annonces_particuliers` (
  `comentaires` varchar(500) DEFAULT NULL,
  `findup` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `id_annonces` int(11) NOT NULL,
  PRIMARY KEY (`id`,`id_annonces`),
  KEY `FK_infos_annonces_particuliers_id_annonces` (`id_annonces`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `infos_annonces_pro`
--

DROP TABLE IF EXISTS `infos_annonces_pro`;
CREATE TABLE IF NOT EXISTS `infos_annonces_pro` (
  `commentaires` varchar(250) DEFAULT NULL,
  `findup` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `id_annonces` int(11) NOT NULL,
  PRIMARY KEY (`id`,`id_annonces`),
  KEY `FK_infos_annonces_pro_id_annonces` (`id_annonces`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `professionnel`
--

DROP TABLE IF EXISTS `professionnel`;
CREATE TABLE IF NOT EXISTS `professionnel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_entreprise` varchar(50) DEFAULT NULL,
  `nom_responsable` varchar(25) DEFAULT NULL,
  `ville` varchar(25) DEFAULT NULL,
  `adresse` varchar(200) DEFAULT NULL,
  `code_postal` varchar(25) DEFAULT NULL,
  `offres_proposes` varchar(25) DEFAULT NULL,
  `duree_de_offre` int(11) DEFAULT NULL,
  `offre_paye` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `mail` varchar(100) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `role` varchar(25) DEFAULT NULL,
  `date_naissance` date DEFAULT NULL,
  `compte_active` tinyint(1) DEFAULT NULL,
  `grade` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `nom`, `prenom`, `mail`, `password`, `role`, `date_naissance`, `compte_active`, `grade`) VALUES
(1, 'part22', 'part22', 'rugor.simon.sio@gmail.com', '99adc231b045331e514a516b4b7680f588e3823213abe901738bc3ad67b2f6fcb3c64efb93d18002588d3ccc1a49efbae1ce20cb43df36b38651f11fa75678e8', 'ROLE_ADMIN', '1980-12-25', 1, 2),
(2, 'part2223', 'part22', 'particulier@gmail.com', 'a144a1194c8eb46bf92087ac205bac9a6e6f50d1c19eb68af143c6eb63e9c6601a29a9c1684a16d5b51748ac5e188fb1686e3b9227442c41a12672b704ad44bd', 'ROLE_PARTICULIER', '1980-12-25', 1, 92),
(4, 'part22', 'part22', 'particulier2@gmail.com', 'a144a1194c8eb46bf92087ac205bac9a6e6f50d1c19eb68af143c6eb63e9c6601a29a9c1684a16d5b51748ac5e188fb1686e3b9227442c41a12672b704ad44bd', 'ROLE_PARTICULIER', '1980-12-25', 1, 19),
(5, 'pro', 'pro', 'pro@gmail.com', '99adc231b045331e514a516b4b7680f588e3823213abe901738bc3ad67b2f6fcb3c64efb93d18002588d3ccc1a49efbae1ce20cb43df36b38651f11fa75678e8', 'ROLE_PRO', '2018-04-10', 1, 2);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `annonces`
--
ALTER TABLE `annonces`
  ADD CONSTRAINT `FK_annonces_id_Categories` FOREIGN KEY (`id_Categories`) REFERENCES `categories` (`id`);

--
-- Contraintes pour la table `infos_annonces_particuliers`
--
ALTER TABLE `infos_annonces_particuliers`
  ADD CONSTRAINT `FK_infos_annonces_particuliers_id` FOREIGN KEY (`id`) REFERENCES `utilisateurs` (`id`),
  ADD CONSTRAINT `FK_infos_annonces_particuliers_id_annonces` FOREIGN KEY (`id_annonces`) REFERENCES `annonces` (`id`);

--
-- Contraintes pour la table `infos_annonces_pro`
--
ALTER TABLE `infos_annonces_pro`
  ADD CONSTRAINT `FK_infos_annonces_pro_id` FOREIGN KEY (`id`) REFERENCES `commercant` (`id`),
  ADD CONSTRAINT `FK_infos_annonces_pro_id_annonces` FOREIGN KEY (`id_annonces`) REFERENCES `annonces` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
